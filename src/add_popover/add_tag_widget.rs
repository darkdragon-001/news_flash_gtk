use crate::app::Action;
use crate::color::ColorRGBA;
use crate::util::Util;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, Entry, Widget};

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::Lazy;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/add_tag_widget.ui")]
    pub struct AddTagWidget {
        #[template_child]
        pub add_tag_button: TemplateChild<Button>,
        #[template_child]
        pub tag_entry: TemplateChild<Entry>,
    }

    impl Default for AddTagWidget {
        fn default() -> Self {
            Self {
                add_tag_button: TemplateChild::default(),
                tag_entry: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddTagWidget {
        const NAME: &'static str = "AddTagWidget";
        type ParentType = Box;
        type Type = super::AddTagWidget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddTagWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> =
                Lazy::new(|| vec![Signal::builder("tag-added", &[], <()>::static_type().into()).build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self, widget: &Self::Type) {
            widget.init();
        }
    }

    impl WidgetImpl for AddTagWidget {}

    impl BoxImpl for AddTagWidget {}
}

glib::wrapper! {
    pub struct AddTagWidget(ObjectSubclass<imp::AddTagWidget>)
        @extends Widget, Box;
}

impl AddTagWidget {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    fn init(&self) {
        let imp = imp::AddTagWidget::from_instance(self);

        let tag_add_button = imp.add_tag_button.get();
        let tag_entry = imp.tag_entry.get();

        // make parse button sensitive if entry contains text and vice versa
        imp.tag_entry
            .connect_changed(clone!(@weak tag_add_button => @default-panic, move |entry| {
                tag_add_button.set_sensitive(!entry.text().as_str().is_empty());
            }));

        // hit enter in entry to add tag
        imp.tag_entry
            .connect_activate(clone!(@weak tag_add_button => @default-panic, move |_entry| {
                if tag_add_button.get_sensitive() {
                    tag_add_button.emit_clicked();
                }
            }));

        imp.add_tag_button.connect_clicked(clone!(
            @weak self as widget,
            @weak tag_entry => @default-panic, move |_button|
        {
            let rgba = ColorRGBA::new(25, 220, 114, 1);
            let color = rgba.to_string_no_alpha();
            if !tag_entry.text().as_str().is_empty() {
                Util::send(Action::AddTag(color, tag_entry.text().as_str().into()));
                widget.emit_by_name::<()>("tag-added", &[]);
            }
        }));
    }

    pub fn reset(&self) {
        let imp = imp::AddTagWidget::from_instance(self);
        imp.tag_entry.set_text("");
    }
}
