mod error;
mod password_login;
mod web_login;

pub use self::password_login::{PasswordLogin, PasswordLoginPrevPage};
pub use self::web_login::{WebLogin, WebLoginPrevPage};
use gtk4::{prelude::*, subclass::prelude::*, Stack};

mod imp {
    use super::*;
    use glib::subclass;
    pub struct LoginPage {
        pub stack: Stack,
        pub password_login: PasswordLogin,
        pub web_login: WebLogin,
    }

    impl Default for LoginPage {
        fn default() -> Self {
            Self {
                stack: Stack::new(),
                password_login: PasswordLogin::new(),
                web_login: WebLogin::new(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LoginPage {
        const NAME: &'static str = "LoginPage";
        type ParentType = gtk4::Box;
        type Type = super::LoginPage;

        fn class_init(_klass: &mut Self::Class) {}

        fn instance_init(_obj: &subclass::InitializingObject<Self>) {}
    }

    impl ObjectImpl for LoginPage {
        fn constructed(&self, gtk_box: &Self::Type) {
            gtk_box.append(&self.stack);
        }
    }

    impl WidgetImpl for LoginPage {}

    impl BoxImpl for LoginPage {}
}

glib::wrapper! {
    pub struct LoginPage(ObjectSubclass<imp::LoginPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl LoginPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }
    pub fn init(&self) {
        let imp = imp::LoginPage::from_instance(self);

        imp.password_login.init();
        imp.web_login.init();

        imp.stack.add_named(&imp.password_login, Some("password_login"));
        imp.stack.add_named(&imp.web_login, Some("oauth_login"));
    }

    pub fn reset(&self) {
        let imp = imp::LoginPage::from_instance(self);
        imp.password_login.reset();
        imp.web_login.reset();
    }

    pub fn set_visible_child_name(&self, name: &str) {
        let imp = imp::LoginPage::from_instance(self);
        imp.stack.set_visible_child_name(name);
    }

    pub fn password_login(&self) -> &PasswordLogin {
        let imp = imp::LoginPage::from_instance(self);
        &imp.password_login
    }

    pub fn web_login(&self) -> &WebLogin {
        let imp = imp::LoginPage::from_instance(self);
        &imp.web_login
    }
}
