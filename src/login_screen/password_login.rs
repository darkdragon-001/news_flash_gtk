use super::error::{LoginScreenError, LoginScreenErrorKind};
use crate::app::Action;
use crate::error_dialog::ErrorDialog;
use crate::i18n::{i18n, i18n_f};
use crate::util::{GtkUtil, Util};
use failure::{Fail, ResultExt};
use glib::{clone, signal::SignalHandlerId, source::Continue};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, CompositeTemplate, Image, InfoBar, Label, ResponseType, Revealer, Stack};
use libadwaita::EntryRow;
use news_flash::models::{
    LoginData, LoginGUI, PasswordLogin as PasswordLoginData, PasswordLoginGUI, PluginID, PluginIcon, PluginInfo,
};
use news_flash::{FeedApiError, FeedApiErrorKind, NewsFlashError, NewsFlashErrorKind};
use parking_lot::RwLock;
use std::sync::Arc;
use std::time::Duration;

#[derive(Debug)]
pub enum PasswordLoginPrevPage {
    Welcome,
    Content(PluginID),
}

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/password_login.ui")]
    pub struct PasswordLogin {
        pub scale_factor: Arc<RwLock<i32>>,

        #[template_child]
        pub logo: TemplateChild<Image>,
        #[template_child]
        pub headline: TemplateChild<Label>,
        #[template_child]
        pub url_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub user_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub pass_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub http_user_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub http_pass_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub http_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub info_bar: TemplateChild<InfoBar>,
        #[template_child]
        pub info_bar_label: TemplateChild<Label>,
        #[template_child]
        pub login_button: TemplateChild<Button>,
        #[template_child]
        pub login_stack: TemplateChild<Stack>,
        #[template_child]
        pub back_button: TemplateChild<Button>,
        #[template_child]
        pub ignore_tls_button: TemplateChild<Button>,
        #[template_child]
        pub error_details_button: TemplateChild<Button>,

        pub info_bar_close_signal: RwLock<Option<SignalHandlerId>>,
        pub info_bar_response_signal: RwLock<Option<SignalHandlerId>>,
        pub url_entry_signal: RwLock<Option<SignalHandlerId>>,
        pub user_entry_signal: RwLock<Option<SignalHandlerId>>,
        pub pass_entry_signal: RwLock<Option<SignalHandlerId>>,
        pub http_user_entry_signal: RwLock<Option<SignalHandlerId>>,
        pub http_pass_entry_signal: RwLock<Option<SignalHandlerId>>,
        pub login_button_signal: RwLock<Option<SignalHandlerId>>,
        pub back_button_signal: RwLock<Option<SignalHandlerId>>,
        pub ignore_tls_signal: RwLock<Option<SignalHandlerId>>,
        pub error_details_signal: RwLock<Option<SignalHandlerId>>,
    }

    impl Default for PasswordLogin {
        fn default() -> Self {
            Self {
                scale_factor: Arc::new(RwLock::new(1)),

                logo: TemplateChild::default(),
                headline: TemplateChild::default(),
                url_entry: TemplateChild::default(),
                user_entry: TemplateChild::default(),
                pass_entry: TemplateChild::default(),
                http_user_entry: TemplateChild::default(),
                http_pass_entry: TemplateChild::default(),
                http_revealer: TemplateChild::default(),
                info_bar: TemplateChild::default(),
                info_bar_label: TemplateChild::default(),
                login_button: TemplateChild::default(),
                login_stack: TemplateChild::default(),
                back_button: TemplateChild::default(),
                ignore_tls_button: TemplateChild::default(),
                error_details_button: TemplateChild::default(),

                info_bar_close_signal: RwLock::new(None),
                info_bar_response_signal: RwLock::new(None),
                url_entry_signal: RwLock::new(None),
                user_entry_signal: RwLock::new(None),
                pass_entry_signal: RwLock::new(None),
                http_user_entry_signal: RwLock::new(None),
                http_pass_entry_signal: RwLock::new(None),
                login_button_signal: RwLock::new(None),
                back_button_signal: RwLock::new(None),
                ignore_tls_signal: RwLock::new(None),
                error_details_signal: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PasswordLogin {
        const NAME: &'static str = "PasswordLogin";
        type ParentType = gtk4::Box;
        type Type = super::PasswordLogin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PasswordLogin {}

    impl WidgetImpl for PasswordLogin {}

    impl BoxImpl for PasswordLogin {}
}

glib::wrapper! {
    pub struct PasswordLogin(ObjectSubclass<imp::PasswordLogin>)
        @extends gtk4::Widget, gtk4::Box;
}

impl PasswordLogin {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::PasswordLogin::from_instance(self);

        *imp.scale_factor.write() = GtkUtil::get_scale(self);
        imp.logo.set_from_icon_name(Some("feed-service-generic"));

        let info_bar = imp.info_bar.get();
        imp.ignore_tls_button.connect_clicked(move |button| {
            Util::send(Action::IgnoreTLSErrors);
            PasswordLogin::hide_info_bar(&info_bar);
            button.set_visible(false);
        });
    }

    pub fn set_service(&self, info: PluginInfo, prev_page: PasswordLoginPrevPage) -> Result<(), LoginScreenError> {
        let imp = imp::PasswordLogin::from_instance(self);

        // setup back button to turn to previous page
        imp.back_button_signal
            .write()
            .replace(imp.back_button.connect_clicked(move |_button| match &prev_page {
                PasswordLoginPrevPage::Welcome => Util::send(Action::ShowWelcomePage),
                PasswordLoginPrevPage::Content(_id) => Util::send(Action::ShowContentPage),
            }));

        // set Icon
        if let Some(icon) = info.icon {
            let texture = match icon {
                PluginIcon::Vector(icon) => {
                    GtkUtil::create_texture_from_bytes(&icon.data, icon.width, icon.height, *imp.scale_factor.read())
                        .context(LoginScreenErrorKind::Icon)?
                }
                PluginIcon::Pixel(icon) => GtkUtil::create_texture_from_pixelicon(&icon, *imp.scale_factor.read())
                    .context(LoginScreenErrorKind::Icon)?,
            };
            imp.logo.set_from_paintable(Some(&texture));
        }

        // set headline
        imp.headline
            .set_text(&i18n_f("Please log into {} and enjoy using NewsFlash", &[&info.name]));

        // setup infobar
        imp.info_bar_close_signal
            .write()
            .replace(imp.info_bar.connect_close(|info_bar| {
                PasswordLogin::hide_info_bar(info_bar);
            }));
        imp.info_bar_response_signal
            .write()
            .replace(imp.info_bar.connect_response(|info_bar, response| {
                if let ResponseType::Close = response {
                    PasswordLogin::hide_info_bar(info_bar);
                }
            }));

        if let LoginGUI::Password(pw_gui_desc) = &info.login_gui {
            // show/hide url & http-auth fields
            imp.url_entry.set_visible(pw_gui_desc.url);
            imp.http_revealer.set_reveal_child(false);

            // set focus to first entry
            if pw_gui_desc.url {
                imp.url_entry.grab_focus();
            } else {
                imp.user_entry.grab_focus();
            }

            // check if 'login' should be clickable
            imp.url_entry_signal
                .write()
                .replace(self.setup_entry(&imp.url_entry, &pw_gui_desc));
            imp.user_entry_signal
                .write()
                .replace(self.setup_entry(&imp.user_entry, &pw_gui_desc));
            imp.pass_entry_signal
                .write()
                .replace(self.setup_entry(&imp.pass_entry, &pw_gui_desc));
            imp.http_user_entry_signal
                .write()
                .replace(self.setup_entry(&imp.http_user_entry, &pw_gui_desc));
            imp.http_pass_entry_signal
                .write()
                .replace(self.setup_entry(&imp.http_pass_entry, &pw_gui_desc));

            // harvest login data
            imp.login_button_signal
                .write()
                .replace(imp.login_button.connect_clicked(clone!(
                    @weak self as page,
                    @strong pw_gui_desc,
                    @strong info.id as plugin_id => @default-panic, move |button|
                {
                    let imp = imp::PasswordLogin::from_instance(&page);
                    imp.login_stack.set_visible_child_name("login_spinner");
                    button.set_sensitive(false);

                    let url: Option<String> = if pw_gui_desc.url {
                        Some(imp.url_entry.text().as_str().to_owned())
                    } else {
                        None
                    };
                    let user = imp.user_entry
                        .text()
                        .as_str()
                        .to_owned();
                    let password = imp.pass_entry
                        .text()
                        .as_str()
                        .to_owned();
                    let http_user: Option<String> = if imp.http_revealer.is_child_revealed() {
                        if imp.http_user_entry.text().is_empty() {
                            None
                        } else {
                            Some(imp.http_user_entry.text().as_str().to_owned())
                        }
                    } else {
                        None
                    };
                    let http_password: Option<String> = if imp.http_revealer.is_child_revealed() {
                        if imp.http_pass_entry.text().is_empty() {
                            None
                        } else {
                            Some(imp.http_pass_entry.text().as_str().to_owned())
                        }
                    } else {
                        None
                    };

                    let login_data = PasswordLoginData {
                        id: plugin_id.clone(),
                        url,
                        user,
                        password,
                        http_user,
                        http_password,
                    };
                    let login_data = LoginData::Password(login_data);
                    Util::send(Action::Login(login_data));
                })));

            return Ok(());
        }

        Err(LoginScreenErrorKind::LoginGUI.into())
    }

    pub fn reset(&self) {
        let imp = imp::PasswordLogin::from_instance(self);

        imp.login_stack.set_visible_child_name("login_label");
        imp.login_button.set_sensitive(true);
        imp.info_bar.set_revealed(false);
        imp.info_bar.set_visible(false);
        imp.url_entry.set_text("");
        imp.user_entry.set_text("");
        imp.pass_entry.set_text("");
        imp.http_user_entry.set_text("");
        imp.http_pass_entry.set_text("");

        GtkUtil::disconnect_signal(imp.info_bar_close_signal.write().take(), &imp.info_bar.get());
        GtkUtil::disconnect_signal(imp.info_bar_response_signal.write().take(), &imp.info_bar.get());
        GtkUtil::disconnect_signal(imp.url_entry_signal.write().take(), &imp.url_entry.get());
        GtkUtil::disconnect_signal(imp.user_entry_signal.write().take(), &imp.user_entry.get());
        GtkUtil::disconnect_signal(imp.pass_entry_signal.write().take(), &imp.pass_entry.get());
        GtkUtil::disconnect_signal(imp.http_user_entry_signal.write().take(), &imp.http_user_entry.get());
        GtkUtil::disconnect_signal(imp.http_pass_entry_signal.write().take(), &imp.http_pass_entry.get());
        GtkUtil::disconnect_signal(imp.login_button_signal.write().take(), &imp.login_button.get());
        GtkUtil::disconnect_signal(imp.back_button_signal.write().take(), &imp.back_button.get());
        GtkUtil::disconnect_signal(imp.ignore_tls_signal.write().take(), &imp.ignore_tls_button.get());
        GtkUtil::disconnect_signal(imp.error_details_signal.write().take(), &imp.error_details_button.get());
    }

    fn hide_info_bar(info_bar: &InfoBar) {
        info_bar.set_revealed(false);
        glib::timeout_add_local(
            Duration::from_millis(200),
            clone!(@weak info_bar => @default-panic, move || {
                info_bar.set_visible(false);
                Continue(false)
            }),
        );
    }

    pub fn show_error(&self, error: NewsFlashError) {
        let imp = imp::PasswordLogin::from_instance(self);

        imp.login_stack.set_visible_child_name("login_label");
        imp.login_button.set_sensitive(true);

        GtkUtil::disconnect_signal(imp.ignore_tls_signal.write().take(), &imp.ignore_tls_button.get());
        GtkUtil::disconnect_signal(imp.error_details_signal.write().take(), &imp.error_details_button.get());

        imp.ignore_tls_button.set_visible(false);
        imp.error_details_button.set_visible(false);
        imp.info_bar_label.set_visible(true);

        match error.kind() {
            NewsFlashErrorKind::Login => {
                if let Some(cause) = error.cause() {
                    if let Some(api_err) = cause.downcast_ref::<FeedApiError>() {
                        match api_err.kind() {
                            FeedApiErrorKind::HTTPAuth => {
                                imp.http_revealer.set_reveal_child(true);
                                imp.login_button.set_sensitive(false);
                                imp.info_bar_label.set_text(&i18n("HTTP authentication required."));
                            }
                            FeedApiErrorKind::TLSCert => {
                                imp.info_bar_label.set_text(&i18n("No valid CA certificate available."));
                                imp.ignore_tls_button.set_visible(true);
                            }
                            FeedApiErrorKind::Login | FeedApiErrorKind::Api | _ => {
                                imp.info_bar_label.set_text(&i18n("Could not log in"));
                            }
                        }
                    }
                }
            }
            _ => imp.info_bar_label.set_text(&i18n("Unknown error.")),
        }

        imp.error_details_button.set_visible(true);
        imp.error_details_signal
            .write()
            .replace(imp.error_details_button.connect_clicked(move |button| {
                let parent = GtkUtil::get_main_window(button)
                    .expect("MainWindow is not a parent of the button in the details of the password-login error.");
                let dialog = ErrorDialog::new();
                dialog.set_transient_for(Some(&parent));
                dialog.set_error(&error);
            }));
        imp.info_bar.set_visible(true);
        imp.info_bar.set_revealed(true);
    }

    pub fn fill(&self, data: PasswordLoginData) {
        let imp = imp::PasswordLogin::from_instance(self);

        imp.info_bar.set_revealed(false);
        imp.info_bar.set_visible(false);
        imp.url_entry.set_text("");
        imp.user_entry.set_text("");
        imp.pass_entry.set_text("");
        imp.http_user_entry.set_text("");
        imp.http_pass_entry.set_text("");

        if let Some(url) = &data.url {
            imp.url_entry.set_text(url);
        }
        imp.user_entry.set_text(&data.user);
        imp.pass_entry.set_text(&data.password);

        if let Some(http_user) = &data.http_user {
            imp.http_user_entry.set_text(http_user);
            imp.http_revealer.set_reveal_child(true);
        }
        if let Some(http_password) = &data.http_password {
            imp.http_pass_entry.set_text(http_password);
        }
    }

    fn setup_entry(&self, entry: &EntryRow, gui_desc: &PasswordLoginGUI) -> SignalHandlerId {
        entry.connect_text_notify(clone!(
            @weak self as page,
            @strong gui_desc => @default-panic, move |_entry|
        {
            let imp = imp::PasswordLogin::from_instance(&page);

            if gui_desc.url && GtkUtil::is_entry_row_emty(&imp.url_entry) {
                imp.login_button.set_sensitive(false);
                return;
            }
            if GtkUtil::is_entry_row_emty(&imp.user_entry) {
                imp.login_button.set_sensitive(false);
                return;
            }
            if GtkUtil::is_entry_row_emty(&imp.pass_entry) {
                imp.login_button.set_sensitive(false);
                return;
            }
            if imp.http_revealer.is_child_revealed() && GtkUtil::is_entry_row_emty(&imp.http_user_entry) {
                imp.login_button.set_sensitive(false);
                return;
            }

            imp.login_button.set_sensitive(true);
        }))
    }
}
