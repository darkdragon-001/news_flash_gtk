use super::error::{LoginScreenError, LoginScreenErrorKind};
use crate::app::Action;
use crate::error_dialog::ErrorDialog;
use crate::i18n::i18n;
use crate::util::{GtkUtil, Util};
use glib::{clone, source::Continue, SignalHandlerId};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, CompositeTemplate, InfoBar, Label, ResponseType};
use news_flash::models::{LoginData, LoginGUI, OAuthData, PluginID, PluginInfo};
use news_flash::{NewsFlashError, NewsFlashErrorKind};
use parking_lot::RwLock;
use std::time::Duration;
use webkit2gtk::{traits::WebViewExt, LoadEvent, WebContext, WebView};

#[derive(Debug)]
pub enum WebLoginPrevPage {
    Welcome,
    Content(PluginID),
}

mod imp {
    use std::sync::Arc;

    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/web_login.ui")]
    pub struct WebLogin {
        pub webview: Arc<RwLock<Option<WebView>>>,

        #[template_child]
        pub info_bar: TemplateChild<InfoBar>,
        #[template_child]
        pub info_bar_label: TemplateChild<Label>,
        #[template_child]
        pub error_details_button: TemplateChild<Button>,
        #[template_child]
        pub back_button: TemplateChild<Button>,

        pub redirect_signal_id: Arc<RwLock<Option<SignalHandlerId>>>,
        pub info_bar_close_signal: RwLock<Option<SignalHandlerId>>,
        pub info_bar_response_signal: RwLock<Option<SignalHandlerId>>,
        pub error_details_signal: RwLock<Option<SignalHandlerId>>,
        pub back_button_signal: RwLock<Option<SignalHandlerId>>,
    }
    impl Default for WebLogin {
        fn default() -> Self {
            Self {
                webview: Arc::new(RwLock::new(None)),

                info_bar: TemplateChild::default(),
                info_bar_label: TemplateChild::default(),
                error_details_button: TemplateChild::default(),
                back_button: TemplateChild::default(),

                redirect_signal_id: Arc::new(RwLock::new(None)),
                info_bar_close_signal: RwLock::new(None),
                info_bar_response_signal: RwLock::new(None),
                error_details_signal: RwLock::new(None),
                back_button_signal: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WebLogin {
        const NAME: &'static str = "WebLogin";
        type ParentType = gtk4::Box;
        type Type = super::WebLogin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WebLogin {}

    impl WidgetImpl for WebLogin {}

    impl BoxImpl for WebLogin {}
}

glib::wrapper! {
    pub struct WebLogin(ObjectSubclass<imp::WebLogin>)
        @extends gtk4::Widget, gtk4::Box;
}

impl WebLogin {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }
    pub fn init(&self) {
        let imp = imp::WebLogin::from_instance(self);

        let context = WebContext::default().expect("Failed to create WebContext");
        let webview = WebView::with_context(&context);
        webview.set_hexpand(true);
        webview.set_vexpand(true);
        self.append(&webview);

        imp.webview.write().replace(webview);
    }

    fn hide_info_bar(info_bar: &InfoBar) {
        info_bar.set_revealed(false);
        glib::timeout_add_local(
            Duration::from_millis(200),
            clone!(@weak info_bar => @default-panic, move || {
                info_bar.set_visible(false);
                Continue(false)
            }),
        );
    }

    pub fn show_error(&self, error: NewsFlashError) {
        let imp = imp::WebLogin::from_instance(self);

        GtkUtil::disconnect_signal(imp.error_details_signal.write().take(), &imp.error_details_button.get());

        match error.kind() {
            NewsFlashErrorKind::Login => imp.info_bar_label.set_text(&i18n("Failed to log in")),
            _ => imp.info_bar_label.set_text(&i18n("Unknown error.")),
        }

        imp.error_details_button.show();
        imp.error_details_signal
            .write()
            .replace(imp.error_details_button.connect_clicked(move |button| {
                let parent = GtkUtil::get_main_window(button).expect("MainWindow is not parent of details button.");
                let dialog = ErrorDialog::new();
                dialog.set_transient_for(Some(&parent));
                dialog.set_error(&error);
            }));

        imp.info_bar.set_visible(true);
        imp.info_bar.set_revealed(true);
    }

    pub fn set_service(&self, info: PluginInfo, prev_page: WebLoginPrevPage) -> Result<(), LoginScreenError> {
        let imp = imp::WebLogin::from_instance(self);

        // setup back button to turn to previous page
        imp.back_button_signal
            .write()
            .replace(imp.back_button.connect_clicked(move |_button| match &prev_page {
                WebLoginPrevPage::Welcome => Util::send(Action::ShowWelcomePage),
                WebLoginPrevPage::Content(_id) => Util::send(Action::ShowContentPage),
            }));

        // setup infobar
        imp.info_bar_close_signal
            .write()
            .replace(imp.info_bar.connect_close(|info_bar| {
                WebLogin::hide_info_bar(info_bar);
            }));
        imp.info_bar_response_signal
            .write()
            .replace(imp.info_bar.connect_response(|info_bar, response| {
                if let ResponseType::Close = response {
                    WebLogin::hide_info_bar(info_bar);
                }
            }));

        if let LoginGUI::OAuth(web_login_desc) = info.login_gui.clone() {
            if let Some(url) = web_login_desc.clone().login_website {
                let webview = imp
                    .webview
                    .read()
                    .clone()
                    .ok_or::<LoginScreenError>(LoginScreenErrorKind::Unknown.into())?;

                webview.load_uri(url.as_str());
                let signal_id = webview.connect_load_changed(clone!(
                    @weak imp.redirect_signal_id as redirect_signal_id => @default-panic, move |webview, event|
                {
                    match event {
                        LoadEvent::Started | LoadEvent::Redirected => {
                            if let Some(redirect_url) = &web_login_desc.catch_redirect {
                                if let Some(uri) = webview.uri() {
                                    if uri.len() > redirect_url.len() && &uri[..redirect_url.len()] == redirect_url {
                                        let oauth_data = OAuthData {
                                            id: info.id.clone(),
                                            url: uri.as_str().to_owned(),
                                        };
                                        let oauth_data = LoginData::OAuth(oauth_data);
                                        GtkUtil::disconnect_signal(redirect_signal_id.write().take(), webview);
                                        webview.stop_loading();
                                        Util::send(Action::Login(oauth_data));
                                    }
                                }
                            }
                        }
                        _ => {
                            // do nothing
                        }
                    }
                }));

                imp.redirect_signal_id.write().replace(signal_id);
                return Ok(());
            }

            return Err(LoginScreenErrorKind::OauthUrl.into());
        }

        Err(LoginScreenErrorKind::LoginGUI.into())
    }

    pub fn reset(&self) {
        let imp = imp::WebLogin::from_instance(self);

        imp.info_bar.set_revealed(false);
        imp.info_bar.set_visible(false);
        GtkUtil::disconnect_signal(imp.info_bar_close_signal.write().take(), &imp.info_bar.get());
        GtkUtil::disconnect_signal(imp.info_bar_response_signal.write().take(), &imp.info_bar.get());
        GtkUtil::disconnect_signal(imp.back_button_signal.write().take(), &imp.back_button.get());
        imp.webview.read().as_ref().map(|w| w.load_plain_text(""));
    }

    pub fn show(&self) {
        let imp = imp::WebLogin::from_instance(self);
        imp.webview.read().as_ref().map(|w| w.show());
    }
}
