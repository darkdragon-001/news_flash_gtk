use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate};
use gtk4::{Align, Box, Label, Widget};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_view_url.ui")]
    pub struct UrlOverlay {
        #[template_child]
        pub label: TemplateChild<Label>,
    }

    impl Default for UrlOverlay {
        fn default() -> Self {
            Self {
                label: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for UrlOverlay {
        const NAME: &'static str = "UrlOverlay";
        type ParentType = Box;
        type Type = super::UrlOverlay;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for UrlOverlay {}

    impl WidgetImpl for UrlOverlay {}

    impl BoxImpl for UrlOverlay {}
}

glib::wrapper! {
    pub struct UrlOverlay(ObjectSubclass<imp::UrlOverlay>)
        @extends Widget, Box;
}

impl UrlOverlay {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn set_url(&self, uri: &str, align: Align) {
        let imp = imp::UrlOverlay::from_instance(self);

        let mut uri = uri.to_owned();
        let max_length = 45;
        if uri.chars().count() > max_length {
            uri = uri.chars().take(max_length).collect::<String>();
            uri.push_str("...");
        }

        imp.label.set_label(&uri);
        imp.label.set_width_chars(uri.chars().count() as i32 - 5);
        imp.label.set_halign(align);
    }

    pub fn reveal(&self, show: bool) {
        if show {
            self.show();
        } else {
            self.hide();
        }
    }
}
