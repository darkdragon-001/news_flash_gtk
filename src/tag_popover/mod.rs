mod popover_tag_gobject;
mod popover_tag_row;

use crate::app::{Action, App};
use crate::undo_action::UndoAction;
use crate::util::{constants, Util};
use gdk4::ModifierType;
use gio::ListStore;
use glib::{clone, subclass};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, ConstantExpression, CustomSorter, EventControllerKey,
    Expression, Inhibit, Label, ListItem, ListView, Popover, PropertyExpression, ScrolledWindow, SearchEntry,
    SignalListItemFactory, SingleSelection, SortListModel, Stack, StringFilter, Widget, INVALID_LIST_POSITION,
};
use news_flash::models::{ArticleID, Tag, TagID};
use parking_lot::RwLock;
use popover_tag_gobject::PopoverTagGObject;
use popover_tag_row::PopoverTagRow;
use std::collections::HashSet;
use std::sync::Arc;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/tag_popover.ui")]
    pub struct TagPopover {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub create_label: TemplateChild<Label>,

        #[template_child]
        pub scroll: TemplateChild<ScrolledWindow>,
        #[template_child]
        pub listview: TemplateChild<ListView>,
        #[template_child]
        pub sorter: TemplateChild<CustomSorter>,
        #[template_child]
        pub sort_list: TemplateChild<SortListModel>,
        #[template_child]
        pub filter: TemplateChild<StringFilter>,
        #[template_child]
        pub list_store: TemplateChild<ListStore>,
        #[template_child]
        pub factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub selection: TemplateChild<SingleSelection>,

        #[template_child]
        pub entry: TemplateChild<SearchEntry>,
        #[template_child]
        pub key_event: TemplateChild<EventControllerKey>,

        pub article_id: Arc<RwLock<Option<ArticleID>>>,
        pub assigned_tags: Arc<RwLock<HashSet<TagID>>>,
        pub tags: Arc<RwLock<Vec<Tag>>>,
        pub ctrl_pressed: Arc<RwLock<bool>>,
    }

    impl Default for TagPopover {
        fn default() -> Self {
            Self {
                stack: TemplateChild::default(),
                create_label: TemplateChild::default(),

                scroll: TemplateChild::default(),
                listview: TemplateChild::default(),
                sorter: TemplateChild::default(),
                sort_list: TemplateChild::default(),
                filter: TemplateChild::default(),
                list_store: TemplateChild::default(),
                factory: TemplateChild::default(),
                selection: TemplateChild::default(),

                entry: TemplateChild::default(),
                key_event: TemplateChild::default(),

                article_id: Arc::new(RwLock::new(None)),
                assigned_tags: Arc::new(RwLock::new(HashSet::new())),
                tags: Arc::new(RwLock::new(Vec::new())),
                ctrl_pressed: Arc::new(RwLock::new(false)),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagPopover {
        const NAME: &'static str = "TagPopover";
        type ParentType = Popover;
        type Type = super::TagPopover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagPopover {}

    impl WidgetImpl for TagPopover {}

    impl PopoverImpl for TagPopover {}
}

glib::wrapper! {
    pub struct TagPopover(ObjectSubclass<imp::TagPopover>)
        @extends Widget, Popover;
}

impl TagPopover {
    pub fn new() -> Self {
        let this = glib::Object::new::<Self>(&[]).unwrap();
        this.init();
        this
    }

    pub fn init(&self) {
        let imp = imp::TagPopover::from_instance(self);

        imp.entry
            .connect_search_changed(clone!(@weak self as this => @default-panic, move |search_entry| {
                let imp = imp::TagPopover::from_instance(&this);
                let query = search_entry.text();
                let query = query.as_str().trim();
                imp.filter.set_search(Some(query));
                imp.create_label.set_text(&format!("Press Enter to create and assign '{}'", query));

                if imp.sort_list.n_items() == 0 {
                    imp.stack.set_visible_child_name(if query.is_empty() { "empty" } else { "create" });
                } else {
                    imp.stack.set_visible_child_name("list");
                }
            }));

        imp.entry
            .connect_activate(clone!(@weak self as this => @default-panic, move |search_entry| {
                let query = search_entry.text();
                let query = query.as_str().trim();

                if query.is_empty() { return; }

                let imp = imp::TagPopover::from_instance(&this);
                let article_id = imp.article_id.read().clone().take().unwrap();
                let ctrl_pressed = *imp.ctrl_pressed.read();
                let no_matches = imp.sort_list.n_items() == 0;
                let exact_match = this.exact_search_match(query);

                if exact_match.is_none() && (ctrl_pressed || no_matches) {
                    // Create new tag
                    Util::send(Action::AddAndAssignTag(constants::TAG_DEFAULT_COLOR.into(), query.into(), article_id.clone()));
                    this.update();
                } else {
                    if let Some(best_match) = this.best_search_match() {
                        // Assign selected tag
                        let is_assigned = best_match.is_assigned();
                        best_match.set_assigned(!is_assigned);

                        let tag_id = (*best_match.tag_id()).clone();
                        let action = if is_assigned {
                            Action::UntagArticle(article_id.clone(), tag_id)
                        } else {
                            Action::TagArticle(article_id.clone(), tag_id)
                        };
                        Util::send(action);
                    }
                }
            }));
        imp.key_event.connect_key_pressed(
            clone!(@weak self as this => @default-panic, move |_controller, key, _keyval, state| {
                let imp = imp::TagPopover::from_instance(&this);
                let ctrl_pressed = state.contains(ModifierType::CONTROL_MASK);
                *imp.ctrl_pressed.write() = ctrl_pressed;

                if ctrl_pressed && key == gdk4::Key::Return {
                    imp.entry.emit_activate();
                } else if key == gdk4::Key::Escape {
                    this.popdown();
                }
                Inhibit(false)
            }),
        );

        imp.factory.connect_setup(
            clone!(@weak self as this => @default-panic, move |_factory, list_item| {
                let row = PopoverTagRow::new();
                list_item.set_child(Some(&row));

                // Create expression describing `list_item->item`
                let list_item_expression = ConstantExpression::new(list_item);
                let article_gobject_expression =
                    PropertyExpression::new(ListItem::static_type(), Some(&list_item_expression), "item");

                // Update title
                let title_expression =
                    PropertyExpression::new(PopoverTagGObject::static_type(), Some(&article_gobject_expression), "title");
                title_expression.bind(&row, "title", Some(&row));

                // Update color
                let color_expression =
                    PropertyExpression::new(PopoverTagGObject::static_type(), Some(&article_gobject_expression), "color");
                color_expression.bind(&row, "color", Some(&row));

                // Update assigned
                let assigned_expression =
                    PropertyExpression::new(PopoverTagGObject::static_type(), Some(&article_gobject_expression), "assigned");
                assigned_expression.bind(&row, "assigned", Some(&row));
            }),
        );
        imp.factory.connect_bind(|_factory, list_item| {
            let tag = list_item.item().unwrap().downcast::<PopoverTagGObject>().unwrap();
            let child = list_item.child().unwrap().downcast::<PopoverTagRow>().unwrap();
            child.bind_model(&tag);
        });
        imp.sorter.set_sort_func(move |obj1, obj2| {
            let tag_1: &PopoverTagGObject = obj1.downcast_ref::<PopoverTagGObject>().unwrap();
            let tag_2: &PopoverTagGObject = obj2.downcast_ref::<PopoverTagGObject>().unwrap();

            let assigned_1 = tag_1.is_assigned();
            let assigned_2 = tag_2.is_assigned();

            let title_1 = tag_1.title();
            let title_2 = tag_2.title();

            if assigned_1 == assigned_2 {
                title_1.cmp(&title_2).into()
            } else {
                assigned_1.cmp(&assigned_2).reverse().into()
            }
        });

        let expr: Option<&Expression> = None;
        let title_expression = PropertyExpression::new(PopoverTagGObject::static_type(), expr, "title");
        imp.filter.set_expression(Some(&title_expression));

        imp.listview.connect_activate(
            clone!(@weak self as this => @default-panic, move |list_view, position| {
                let imp = imp::TagPopover::from_instance(&this);
                let model = list_view.model().expect("The model has to exist.");
                let tag_gobject = model
                    .item(position)
                    .expect("The item has to exist.")
                    .downcast::<PopoverTagGObject>()
                    .expect("The item has to be an `PopoverTagGObject`.");

                let is_assigned = tag_gobject.is_assigned();
                tag_gobject.set_assigned(!is_assigned);
                if let Some(article_id) = imp.article_id.read().as_ref() {
                    let tag_id = (*tag_gobject.tag_id()).clone();
                    let action = if is_assigned {
                        Action::UntagArticle(article_id.clone(), tag_id)
                    } else {
                        Action::TagArticle(article_id.clone(), tag_id)
                    };
                    Util::send(action);
                };
            }),
        );

        self.connect_show(|this| {
            this.update();
        });
    }

    fn reset_view(&self) {
        let imp = imp::TagPopover::from_instance(self);
        imp.entry.set_text("");
        if self.is_visible() {
            let _ = imp.entry.grab_focus();
        }

        if imp.list_store.n_items() == 0 {
            imp.stack.set_visible_child_name("empty");
        } else {
            imp.stack.set_visible_child_name("list");
        }

        let scroll = imp.scroll.clone();
        glib::timeout_add_local(std::time::Duration::from_millis(20), move || {
            scroll.vadjustment().set_value(0.0);
            Continue(false)
        });
    }

    pub fn set_article_id(&self, article_id: &ArticleID) {
        let imp = imp::TagPopover::from_instance(self);
        imp.article_id.write().replace(article_id.clone());
        self.update();
    }

    fn update(&self) {
        let imp = imp::TagPopover::from_instance(self);
        imp.list_store.remove_all();
        imp.assigned_tags.write().clear();

        let processing_undo_actions = App::default().processing_undo_actions();
        let mut blacklisted_tags: HashSet<TagID> = processing_undo_actions
            .read()
            .iter()
            .filter_map(|action| {
                if let UndoAction::DeleteTag(tag_id, _) = action {
                    Some(tag_id.clone())
                } else {
                    None
                }
            })
            .collect();
        if let Some(current_undo_action) = App::default().current_undo_action() {
            if let UndoAction::DeleteTag(tag_id, _) = current_undo_action {
                blacklisted_tags.insert(tag_id);
            }
        }

        if let Some(news_flash) = App::default().news_flash().read().as_ref() {
            if let Some(article_id) = imp.article_id.read().as_ref() {
                if let Ok(tags) = news_flash.get_tags_of_article(article_id) {
                    for tag in tags {
                        if blacklisted_tags.contains(&tag.tag_id) {
                            continue;
                        }
                        imp.assigned_tags.write().insert(tag.tag_id.clone());
                    }
                }
            }

            if let Ok((tags, _taggings)) = news_flash.get_tags() {
                for tag in tags {
                    imp.list_store.append(&PopoverTagGObject::from_model(
                        &tag,
                        imp.assigned_tags.read().contains(&tag.tag_id),
                    ));
                    imp.tags.write().push(tag);
                }
            }
        }

        self.reset_view();
    }

    fn exact_search_match(&self, search_query: &str) -> Option<PopoverTagGObject> {
        let imp = imp::TagPopover::from_instance(self);
        let n_items = imp.sort_list.n_items();

        for i in 0..n_items {
            if let Some(tag_gobject) = imp
                .sort_list
                .item(i)
                .as_ref()
                .map(move |obj| obj.downcast_ref::<PopoverTagGObject>().unwrap())
            {
                if tag_gobject.title() == search_query {
                    return Some(tag_gobject.clone());
                }
            }
        }
        None
    }

    fn best_search_match(&self) -> Option<PopoverTagGObject> {
        let imp = imp::TagPopover::from_instance(self);
        let selected = imp.selection.selected();
        let selected = if selected == INVALID_LIST_POSITION { 0 } else { selected };

        imp.sort_list
            .item(selected)
            .map(move |obj| obj.downcast_ref::<PopoverTagGObject>().unwrap().clone())
    }
}
