use super::error::{ContentPageError, ContentPageErrorKind};
use crate::account_popover::AccountPopover;
use crate::add_popover::AddPopover;
use crate::app::App;
use crate::color::ColorRGBA;
use crate::i18n::i18n;
use crate::sidebar::SideBar;
use crate::util::{GtkUtil, Util};
use failure::ResultExt;
use gio::Menu;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Image, MenuButton, ToggleButton};
use libadwaita::HeaderBar;
use news_flash::models::{PluginCapabilities, PluginID, PluginIcon};
use news_flash::NewsFlash;
use parking_lot::RwLock;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/sidebar_column.ui")]
    pub struct SidebarColumn {
        #[template_child]
        pub headerbar: TemplateChild<HeaderBar>,
        #[template_child]
        pub account_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub account_button_image: TemplateChild<Image>,
        #[template_child]
        pub add_button: TemplateChild<MenuButton>,
        #[template_child]
        pub menu_button: TemplateChild<MenuButton>,
        #[template_child]
        pub sidebar: TemplateChild<SideBar>,

        pub add_popover: AddPopover,
        pub account_popover: RwLock<Option<AccountPopover>>,
    }

    impl Default for SidebarColumn {
        fn default() -> Self {
            Self {
                headerbar: TemplateChild::default(),
                account_button: TemplateChild::default(),
                account_button_image: TemplateChild::default(),
                add_button: TemplateChild::default(),
                menu_button: TemplateChild::default(),
                sidebar: TemplateChild::default(),

                add_popover: AddPopover::new(),
                account_popover: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SidebarColumn {
        const NAME: &'static str = "SidebarColumn";
        type ParentType = gtk4::Box;
        type Type = super::SidebarColumn;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SidebarColumn {}

    impl WidgetImpl for SidebarColumn {}

    impl BoxImpl for SidebarColumn {}
}

glib::wrapper! {
    pub struct SidebarColumn(ObjectSubclass<imp::SidebarColumn>)
        @extends gtk4::Widget, gtk4::Box;
}

impl SidebarColumn {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::SidebarColumn::from_instance(self);

        imp.sidebar.init();

        let account_popover = AccountPopover::new(&*imp.account_button);
        let account_button = imp.account_button.get();
        account_popover.widget.connect_closed(move |_popover| {
            account_button.set_active(false);
        });
        imp.account_button.connect_clicked(
            clone!(@weak account_popover.widget as popover => @default-panic, move |toggle_button| {
                if toggle_button.is_active() {
                    popover.popup();
                } else {
                    popover.popdown();
                }
            }),
        );
        imp.account_popover.write().replace(account_popover);
        imp.add_button.set_popover(Some(&imp.add_popover));

        self.setup_menu_button();
    }

    fn account_popover(&self) -> AccountPopover {
        let imp = imp::SidebarColumn::from_instance(self);
        imp.account_popover
            .read()
            .clone()
            .expect("SidebarColumn not initialized")
    }

    pub fn sidebar(&self) -> &SideBar {
        let imp = imp::SidebarColumn::from_instance(self);
        &imp.sidebar
    }

    pub fn headerbar(&self) -> &HeaderBar {
        let imp = imp::SidebarColumn::from_instance(self);
        &imp.headerbar
    }

    fn setup_menu_button(&self) {
        let imp = imp::SidebarColumn::from_instance(self);

        let about_model = Menu::new();
        about_model.append(Some(&i18n("Shortcuts")), Some("win.shortcut-window"));
        about_model.append(Some(&i18n("About")), Some("win.about-window"));
        about_model.append(Some(&i18n("Quit")), Some("win.quit-application"));

        let im_export_model = Menu::new();
        im_export_model.append(Some(&i18n("Import OPML")), Some("win.import-opml"));
        im_export_model.append(Some(&i18n("Export OPML")), Some("win.export-opml"));

        let main_model = Menu::new();
        main_model.append(Some(&i18n("Preferences")), Some("win.settings"));
        main_model.append(Some(&i18n("Discover Feeds")), Some("win.discover"));
        main_model.append_section(None, &im_export_model);
        main_model.append_section(None, &about_model);

        imp.menu_button.set_menu_model(Some(&main_model));
    }

    pub fn set_account(&self, id: &PluginID, user_name: Option<&str>) -> Result<(), ContentPageError> {
        let imp = imp::SidebarColumn::from_instance(self);
        let scale = GtkUtil::get_scale(&*imp.account_button_image);
        imp.account_button_image
            .set_from_icon_name(Some("feed-service-generic"));
        let mut vector_data = None;
        let user;

        let list = NewsFlash::list_backends();
        if let Some(info) = list.get(id) {
            user = match user_name {
                Some(user_name) => user_name.into(),
                None => info.name.clone(),
            };
            if let Some(plugin_icon) = &info.icon_symbolic {
                match plugin_icon {
                    PluginIcon::Vector(vector_icon) => {
                        let style_context = self.style_context();
                        let color = style_context.lookup_color("theme_fg_color");
                        let color = color.unwrap_or(style_context.color());
                        let color = ColorRGBA::from_normalized(
                            color.red() as f64,
                            color.green() as f64,
                            color.blue() as f64,
                            color.alpha() as f64,
                        );
                        let colored_data =
                            Util::symbolic_icon_set_color(&vector_icon.data, &color.to_string_no_alpha())
                                .context(ContentPageErrorKind::MetaData)?;
                        let texture = GtkUtil::create_texture_from_bytes(&colored_data, 16, 16, scale)
                            .context(ContentPageErrorKind::MetaData)?;
                        imp.account_button_image.set_from_paintable(Some(&texture));

                        let mut vector_icon = vector_icon.clone();
                        vector_icon.data = colored_data;
                        vector_data = Some(vector_icon);
                    }
                    PluginIcon::Pixel(_icon) => {
                        log::warn!("Pixel based icon not valid for account button");
                    }
                }
            }
        } else {
            log::warn!("Try loading branding failed. Backend '{}' not found.", id);
            return Err(ContentPageErrorKind::MetaData.into());
        }

        self.account_popover().set_account(vector_data, &user);

        Ok(())
    }

    pub fn update_features(&self) {
        let imp = imp::SidebarColumn::from_instance(self);
        imp.add_button.set_sensitive(
            !App::default().content_page_state().read().get_offline()
                && App::default()
                    .features()
                    .read()
                    .contains(PluginCapabilities::ADD_REMOVE_FEEDS),
        );
    }

    pub fn set_offline(&self, offline: bool) {
        let imp = imp::SidebarColumn::from_instance(self);

        imp.add_button.set_sensitive(
            !offline
                && App::default()
                    .features()
                    .read()
                    .contains(PluginCapabilities::ADD_REMOVE_FEEDS),
        );
        imp.sidebar.feed_list().update_offline();
    }

    pub fn refresh_app_popover_features(&self) {
        let imp = imp::SidebarColumn::from_instance(self);
        imp.add_popover.parse_features();
    }
}
