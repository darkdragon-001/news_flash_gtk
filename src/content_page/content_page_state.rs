use crate::content_page::ArticleListMode;
use crate::sidebar::models::SidebarSelection;

#[derive(Clone, Debug)]
pub struct ContentPageState {
    sidebar_selection: SidebarSelection,
    article_list_mode: ArticleListMode,
    search_term: Option<String>,
    offline: bool,
    prefer_scraped_content: bool,
    ongoing_article_scrap: bool,
    video_fullscreen: bool,
}

const ARTICLE_LIST_PAGE_SIZE: i64 = 20;

impl ContentPageState {
    pub fn new() -> Self {
        ContentPageState {
            sidebar_selection: SidebarSelection::All,
            article_list_mode: ArticleListMode::All,
            search_term: None,
            offline: false,
            prefer_scraped_content: false,
            ongoing_article_scrap: false,
            video_fullscreen: false,
        }
    }

    pub fn page_size() -> i64 {
        ARTICLE_LIST_PAGE_SIZE
    }

    pub fn get_sidebar_selection(&self) -> &SidebarSelection {
        &self.sidebar_selection
    }

    pub fn set_sidebar_selection(&mut self, selection: SidebarSelection) {
        self.sidebar_selection = selection;
    }

    pub fn get_article_list_mode(&self) -> &ArticleListMode {
        &self.article_list_mode
    }

    pub fn set_article_list_mode(&mut self, header: ArticleListMode) {
        self.article_list_mode = header;
    }

    pub fn get_search_term(&self) -> &Option<String> {
        &self.search_term
    }

    pub fn set_search_term(&mut self, search_term: Option<String>) {
        self.search_term = search_term;
    }

    pub fn set_offline(&mut self, offline: bool) {
        self.offline = offline;
    }

    pub fn get_offline(&self) -> bool {
        self.offline
    }

    pub fn set_prefer_scraped_content(&mut self, pref: bool) {
        self.prefer_scraped_content = pref;
    }

    pub fn get_prefer_scraped_content(&self) -> bool {
        self.prefer_scraped_content
    }

    pub fn started_scraping_article(&mut self) {
        self.ongoing_article_scrap = true;
    }

    pub fn finished_scraping_article(&mut self) {
        self.ongoing_article_scrap = false;
    }

    pub fn is_article_scrap_ongoing(&self) -> bool {
        self.ongoing_article_scrap
    }

    pub fn get_video_fullscreen(&self) -> bool {
        self.video_fullscreen
    }

    pub fn set_video_fullscreen(&mut self, video_fullscreen: bool) {
        self.video_fullscreen = video_fullscreen;
    }
}

impl PartialEq for ContentPageState {
    fn eq(&self, other: &ContentPageState) -> bool {
        if self.sidebar_selection != other.sidebar_selection {
            return false;
        }
        if self.article_list_mode != other.article_list_mode {
            return false;
        }
        match &self.search_term {
            Some(self_search_term) => match &other.search_term {
                Some(other_search_term) => {
                    if self_search_term != other_search_term {
                        return false;
                    }
                }
                None => return false,
            },
            None => match &other.search_term {
                Some(_) => return false,
                None => {}
            },
        }
        true
    }
}
