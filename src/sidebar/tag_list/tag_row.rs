use crate::app::App;
use crate::sidebar::tag_list::models::TagGObject;
use crate::util::{constants, GtkUtil};
use gio::{Menu, MenuItem};
use glib::{clone, subclass::*, ParamFlags, ParamSpec, ParamSpecString, SignalHandlerId, Value};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, DrawingArea, Label};
use gtk4::{GestureClick, GestureLongPress, PopoverMenu, PositionType};
use news_flash::models::{PluginCapabilities, TagID};
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use std::str;
use std::sync::Arc;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder("activated", &[TagRow::static_type().into()], <()>::static_type().into()).build()]
});

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/tag.ui")]
    pub struct TagRow {
        pub id: RwLock<Arc<TagID>>,
        pub color: Arc<RwLock<String>>,
        pub popover: Arc<RwLock<Option<PopoverMenu>>>,
        pub right_click_source: RwLock<Option<SignalHandlerId>>,
        pub long_press_source: RwLock<Option<SignalHandlerId>>,

        #[template_child]
        pub tag_title: TemplateChild<Label>,
        #[template_child]
        pub tag_color: TemplateChild<DrawingArea>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
        #[template_child]
        pub row_long_press: TemplateChild<GestureLongPress>,
    }

    impl Default for TagRow {
        fn default() -> Self {
            TagRow {
                id: RwLock::new(Arc::new(TagID::new(""))),
                color: Arc::new(RwLock::new(constants::TAG_DEFAULT_COLOR.into())),
                popover: Arc::new(RwLock::new(None)),
                right_click_source: RwLock::new(None),
                long_press_source: RwLock::new(None),

                tag_title: TemplateChild::default(),
                tag_color: TemplateChild::default(),
                row_click: TemplateChild::default(),
                row_activate: TemplateChild::default(),
                row_long_press: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagRow {
        const NAME: &'static str = "TagRow";
        type ParentType = gtk4::Box;
        type Type = super::TagRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new("title", "title", "title", None, ParamFlags::READWRITE),
                    ParamSpecString::new("color", "color", "color", None, ParamFlags::READWRITE),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "title" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.tag_title.set_label(&input);
                }
                "color" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    *self.color.write() = input;
                    self.tag_color.queue_draw();
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "title" => obj.get_title().to_value(),
                "color" => self.color.read().clone().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for TagRow {}

    impl BoxImpl for TagRow {}
}

glib::wrapper! {
    pub struct TagRow(ObjectSubclass<imp::TagRow>)
        @extends gtk4::Widget, gtk4::Box;
}

impl TagRow {
    pub fn new() -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        row.init();
        row
    }

    fn init(&self) {
        let imp = imp::TagRow::from_instance(self);

        imp.row_activate.connect_released(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            this.activate();
        }));
    }

    pub fn bind_model(&self, model: &TagGObject) {
        let imp = imp::TagRow::from_instance(self);
        let is_same_tag = *imp.id.read() == model.tag_id();

        let features = App::default().features();
        let support_mutation = features.read().contains(PluginCapabilities::SUPPORT_TAGS);

        *imp.id.write() = model.tag_id();
        *imp.color.write() = model.color();

        if support_mutation && imp.right_click_source.read().is_none() {
            self.setup_right_click();
        }

        imp.tag_color.set_draw_func(
            clone!(@strong imp.color as color => @default-panic, move |_drawing_area, ctx, _width, _height| {
                GtkUtil::draw_color_cirlce(&ctx, &*color.read(), None);
            }),
        );

        if !is_same_tag {
            imp.tag_title.set_label(&model.title());
        }
    }

    fn get_title(&self) -> String {
        let imp = imp::TagRow::from_instance(self);
        imp.tag_title.text().as_str().to_string()
    }

    fn setup_right_click(&self) {
        let imp = imp::TagRow::from_instance(self);

        imp.right_click_source
            .write()
            .replace(imp.row_click.connect_released(clone!(
                @weak self as this => @default-panic, move |_gesture, times, _x, _y|
            {
                if times != 1 {
                    return
                }

                if App::default().content_page_state().read().get_offline() {
                    return
                }

                let imp = imp::TagRow::from_instance(&this);
                if imp.popover.read().is_none() {
                    imp.popover.write().replace(this.setup_context_popover());
                }

                if let Some(popover) = imp.popover.read().as_ref() {
                    popover.popup();
                };
            })));

        imp.long_press_source
            .write()
            .replace(imp.row_long_press.connect_pressed(clone!(
                @weak self as this => @default-panic, move |_gesture, _x, _y|
            {
                if App::default().content_page_state().read().get_offline() {
                    return
                }

                let imp = imp::TagRow::from_instance(&this);
                if imp.popover.read().is_none() {
                    imp.popover.write().replace(this.setup_context_popover());
                }

                if let Some(popover) = imp.popover.read().as_ref() {
                    popover.popup();
                };
            })));
    }

    fn setup_context_popover(&self) -> PopoverMenu {
        let imp = imp::TagRow::from_instance(self);
        let tag_id = (**imp.id.read()).clone();

        let model = Menu::new();
        let rename_item = MenuItem::new(Some("Edit"), None);
        let delete_item = MenuItem::new(Some("Delete"), None);

        rename_item.set_action_and_target_value(Some("win.edit-tag-dialog"), Some(&tag_id.as_str().to_variant()));

        let tuple_variant = (tag_id.as_str().to_string(), self.get_title()).to_variant();
        delete_item.set_action_and_target_value(Some("win.enqueue-delete-tag"), Some(&tuple_variant));

        model.append_item(&rename_item);
        model.append_item(&delete_item);

        let popover = PopoverMenu::from_model(Some(&model));
        popover.set_parent(self);
        popover.set_position(PositionType::Bottom);
        popover
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()]);
    }
}
