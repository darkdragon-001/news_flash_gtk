mod feed_list;
pub mod models;
mod tag_list;

use crate::app::Action;
use crate::util::{GtkUtil, Util};
pub use feed_list::models::{FeedListDndAction, FeedListItemID, FeedListTree};
use feed_list::FeedList;
use glib::{clone, source::Continue, SourceId};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, EventControllerMotion, GestureClick, Image, Label,
    ListBox, ListBoxRow, Revealer, ScrolledWindow, Widget,
};
pub use models::SidebarIterateItem;
use models::SidebarSelection;
use parking_lot::RwLock;
use std::sync::Arc;
use std::time::Duration;
pub use tag_list::models::TagListModel;
use tag_list::TagList;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/sidebar.ui")]
    pub struct SideBar {
        #[template_child]
        pub sidebar_scroll: TemplateChild<ScrolledWindow>,

        #[template_child]
        pub categories_event_box: TemplateChild<Box>,
        #[template_child]
        pub categories_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub categories_click: TemplateChild<GestureClick>,

        #[template_child]
        pub tags_event_box: TemplateChild<Box>,
        #[template_child]
        pub tags_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub tags_click: TemplateChild<GestureClick>,

        #[template_child]
        pub tags_box: TemplateChild<Box>,
        #[template_child]
        pub all_articles_fake_list: TemplateChild<ListBox>,
        #[template_child]
        pub all_articles_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub item_count_all_label: TemplateChild<Label>,
        #[template_child]
        pub feed_list: TemplateChild<FeedList>,
        #[template_child]
        pub tag_list: TemplateChild<TagList>,
        #[template_child]
        pub categories_expander: TemplateChild<Image>,
        #[template_child]
        pub tags_expander: TemplateChild<Image>,
        #[template_child]
        pub categories_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub tags_revealer: TemplateChild<Revealer>,

        pub item_count_all: RwLock<i64>,
        pub scale_factor: RwLock<i32>,
        pub selection: Arc<RwLock<SidebarSelection>>,
        pub delayed_all_selection: RwLock<Option<SourceId>>,

        pub delay_next_feedlist_activation: RwLock<bool>,
        pub delayed_feedlist_selection: RwLock<Option<SourceId>>,

        pub delay_next_taglist_activation: RwLock<bool>,
        pub delayed_taglist_selection: RwLock<Option<SourceId>>,
    }

    impl Default for SideBar {
        fn default() -> Self {
            Self {
                sidebar_scroll: TemplateChild::default(),

                categories_event_box: TemplateChild::default(),
                categories_motion: TemplateChild::default(),
                categories_click: TemplateChild::default(),

                tags_event_box: TemplateChild::default(),
                tags_motion: TemplateChild::default(),
                tags_click: TemplateChild::default(),

                tags_box: TemplateChild::default(),
                all_articles_fake_list: TemplateChild::default(),
                all_articles_row: TemplateChild::default(),
                item_count_all_label: TemplateChild::default(),
                feed_list: TemplateChild::default(),
                tag_list: TemplateChild::default(),
                categories_expander: TemplateChild::default(),
                tags_expander: TemplateChild::default(),
                categories_revealer: TemplateChild::default(),
                tags_revealer: TemplateChild::default(),

                item_count_all: RwLock::new(0),
                scale_factor: RwLock::new(1),
                selection: Arc::new(RwLock::new(SidebarSelection::All)),
                delayed_all_selection: RwLock::new(None),

                delay_next_feedlist_activation: RwLock::new(false),
                delayed_feedlist_selection: RwLock::new(None),

                delay_next_taglist_activation: RwLock::new(false),
                delayed_taglist_selection: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SideBar {
        const NAME: &'static str = "SideBar";
        type ParentType = Box;
        type Type = super::SideBar;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SideBar {}

    impl WidgetImpl for SideBar {}

    impl BoxImpl for SideBar {}
}

glib::wrapper! {
    pub struct SideBar(ObjectSubclass<imp::SideBar>)
        @extends Widget, Box;
}

impl SideBar {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::SideBar::from_instance(self);
        imp.all_articles_fake_list.select_row(Some(&*imp.all_articles_row));

        imp.feed_list.init(&imp.sidebar_scroll);
        imp.tag_list.init();

        imp.all_articles_fake_list.connect_row_activated(
            clone!(@weak self as this => @default-panic, move |_list, _row| {
                let imp = imp::SideBar::from_instance(&this);
                Util::send(Action::SidebarSelection((*imp.selection.read()).clone()));
            }),
        );

        imp.all_articles_fake_list.connect_row_selected(
            clone!(@weak self as this => @default-panic, move |_all_fake_list, row|
            {
                // do nothing if selection was cleared
                if row.is_none() {
                    return;
                }

                let imp = imp::SideBar::from_instance(&this);

                // deselect feed_list_handle & tag_list
                imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);
                imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                *imp.selection.write() = SidebarSelection::All;
            }),
        );

        imp.feed_list.listview().connect_activate(
            clone!(@weak self as this => @default-panic, move |_listview, _pos| {
                let imp = imp::SideBar::from_instance(&this);
                Util::send(Action::SidebarSelection((*imp.selection.read()).clone()));
            }),
        );

        imp.feed_list.selection().connect_selection_changed(
            clone!(@weak self as this => @default-panic, move |selection_model, _pos, _n_items|
            {
                // do nothing if selection was cleared
                if selection_model.selected_item().is_none() {
                    return;
                }

                let imp = imp::SideBar::from_instance(&this);

                // deselect 'all' & tag_list
                imp.all_articles_fake_list.unselect_all();
                imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                if let Some((item, title, _item_count)) = imp.feed_list.get_selection() {
                    let selection = SidebarSelection::from_feed_list_selection(item, title);
                    *imp.selection.write() = selection;
                }

                let selected_pos = selection_model.selected();

                // activate selected item
                // this emulates "single-click-activate" without the select on hover
                if *imp.delay_next_feedlist_activation.read() {
                    GtkUtil::remove_source(imp.delayed_feedlist_selection.write().take());
                    *imp.delayed_feedlist_selection.write() = Some(glib::timeout_add_local(
                        Duration::from_millis(300),
                        clone!(@weak this => @default-panic, move || {
                            let imp = imp::SideBar::from_instance(&this);
                            imp.delayed_feedlist_selection.write().take();
                            imp.feed_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                            Continue(false)
                        }),
                    ));
                } else {
                    imp.feed_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                }
            }),
        );

        imp.tag_list
            .listview()
            .connect_activate(clone!(@weak self as this => @default-panic, move |_listview, _pos| {
                let imp = imp::SideBar::from_instance(&this);
                Util::send(Action::SidebarSelection((*imp.selection.read()).clone()));
            }));

        imp.tag_list.selection().connect_selection_changed(
            clone!(@weak self as this => @default-panic, move |selection_model, _pos, _n_items|
            {
                // do nothing if selection was cleared
                if selection_model.selected_item().is_none() {
                    return;
                }

                let imp = imp::SideBar::from_instance(&this);

                // deselect 'all' & feed_list
                imp.all_articles_fake_list.unselect_all();
                imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                if let Some((item, title)) = imp.tag_list.get_selection() {
                    let selection = SidebarSelection::from_tag_list_selection(item, title);
                    *imp.selection.write() = selection;
                }

                let selected_pos = selection_model.selected();

                // activate selected item
                // this emulates "single-click-activate" without the select on hover
                if *imp.delay_next_taglist_activation.read() {
                    GtkUtil::remove_source(imp.delayed_taglist_selection.write().take());
                    *imp.delayed_taglist_selection.write() = Some(glib::timeout_add_local(
                        Duration::from_millis(300),
                        clone!(@weak this => @default-panic, move || {
                            let imp = imp::SideBar::from_instance(&this);
                            imp.delayed_taglist_selection.write().take();
                            imp.tag_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                            Continue(false)
                        }),
                    ));
                } else {
                    imp.tag_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                }
            }),
        );

        *imp.scale_factor.write() = GtkUtil::get_scale(self);

        Self::setup_expander(
            &imp.categories_motion,
            &imp.categories_click,
            &imp.categories_event_box,
            &imp.categories_expander,
            &imp.categories_revealer,
        );
        Self::setup_expander(
            &imp.tags_motion,
            &imp.tags_click,
            &imp.tags_event_box,
            &imp.tags_expander,
            &imp.tags_revealer,
        );
    }

    pub fn feed_list(&self) -> &FeedList {
        let imp = imp::SideBar::from_instance(self);
        &imp.feed_list
    }

    pub fn tag_list(&self) -> &TagList {
        let imp = imp::SideBar::from_instance(self);
        &imp.tag_list
    }

    pub fn update_feedlist(&self, tree: FeedListTree) {
        let imp = imp::SideBar::from_instance(self);
        imp.feed_list.update(tree);
    }

    pub fn update_taglist(&self, list: TagListModel) {
        let imp = imp::SideBar::from_instance(self);
        imp.tag_list.update(list);
    }

    pub fn hide_taglist(&self) {
        let imp = imp::SideBar::from_instance(self);
        imp.tags_box.hide();
    }

    pub fn show_taglist(&self) {
        let imp = imp::SideBar::from_instance(self);
        imp.tags_box.show();
    }

    pub fn update_all(&self, item_count: i64) {
        let imp = imp::SideBar::from_instance(self);
        *imp.item_count_all.write() = item_count;
        imp.item_count_all_label.set_text(&format!("{}", item_count));
        imp.item_count_all_label.set_visible(item_count > 0);
    }

    fn setup_expander(
        motion_event: &EventControllerMotion,
        click_gesture: &GestureClick,
        row: &Box,
        expander: &Image,
        revealer: &Revealer,
    ) {
        motion_event.connect_enter(clone!(@weak row => @default-panic, move |_controller, _x, _y| {
            let context = row.style_context();
            context.add_class("highlight");
        }));
        motion_event.connect_leave(clone!(@weak row => @default-panic, move |_controller| {
            let context = row.style_context();
            context.remove_class("highlight");
        }));

        click_gesture.connect_released(clone!(
            @weak expander,
            @weak revealer => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }
            Self::toggle_expand_list(&revealer, &expander);
        }));
    }

    fn expand_list(revealer: &Revealer, expander: &Image) {
        if !revealer.is_child_revealed() {
            Self::toggle_expand_list(revealer, expander);
        }
    }

    fn toggle_expand_list(revealer: &Revealer, expander: &Image) {
        let context = expander.style_context();
        if revealer.is_child_revealed() {
            context.add_class("backward-arrow-collapsed");
            context.remove_class("backward-arrow-expanded");
            revealer.set_reveal_child(false);
        } else {
            context.remove_class("backward-arrow-collapsed");
            context.add_class("backward-arrow-expanded");
            revealer.set_reveal_child(true);
        }
    }

    pub fn select_all_button_no_update(&self) {
        let imp = imp::SideBar::from_instance(self);
        *imp.selection.write() = SidebarSelection::All;
        GtkUtil::remove_source(imp.delayed_all_selection.write().take());
        imp.all_articles_fake_list.select_row(Some(&*imp.all_articles_row));
    }

    pub fn select_all_button(&self) {
        let imp = imp::SideBar::from_instance(self);

        *imp.selection.write() = SidebarSelection::All;
        imp.all_articles_fake_list.select_row(Some(&*imp.all_articles_row));

        GtkUtil::remove_source(imp.delayed_all_selection.write().take());
        *imp.delayed_all_selection.write() = Some(glib::timeout_add_local(
            Duration::from_millis(300),
            clone!(@weak self as this => @default-panic, move || {
                let imp = imp::SideBar::from_instance(&this);
                Util::send(Action::SidebarSelection(SidebarSelection::All));
                imp.delayed_all_selection.write().take();
                Continue(false)
            }),
        ));
    }

    pub fn select_next_item(&self) {
        let imp = imp::SideBar::from_instance(self);

        let select_next = match *imp.selection.read() {
            SidebarSelection::All => SidebarIterateItem::FeedListSelectFirstItem,
            SidebarSelection::FeedList(_, _) => imp.feed_list.calc_next_item(),
            SidebarSelection::Tag(_, _) => imp.tag_list.get_next_item(),
        };
        self.select_item(select_next)
    }

    pub fn select_prev_item(&self) {
        let imp = imp::SideBar::from_instance(self);

        let select_next = match *imp.selection.read() {
            SidebarSelection::All => SidebarIterateItem::TagListSelectLastItem,
            SidebarSelection::FeedList(_, _) => imp.feed_list.calc_prev_item(),
            SidebarSelection::Tag(_, _) => imp.tag_list.get_prev_item(),
        };
        self.select_item(select_next)
    }

    fn select_item(&self, selection: SidebarIterateItem) {
        let imp = imp::SideBar::from_instance(self);

        self.deselect();

        match selection {
            SidebarIterateItem::SelectAll => {
                self.select_all_button();
            }
            SidebarIterateItem::SelectFeedListItem(item_id) => {
                *imp.delay_next_feedlist_activation.write() = true;
                imp.feed_list.set_selection(item_id);
            }
            SidebarIterateItem::FeedListSelectFirstItem => {
                Self::expand_list(&imp.categories_revealer, &imp.categories_expander);
                if let Some(item) = imp.feed_list.get_first_item() {
                    *imp.delay_next_feedlist_activation.write() = true;
                    imp.feed_list.set_selection(item);
                }
            }
            SidebarIterateItem::FeedListSelectLastItem => {
                Self::expand_list(&imp.categories_revealer, &imp.categories_expander);
                if let Some(item) = imp.feed_list.get_last_item() {
                    *imp.delay_next_feedlist_activation.write() = true;
                    imp.feed_list.set_selection(item);
                }
            }
            SidebarIterateItem::SelectTagList(id) => {
                *imp.delay_next_taglist_activation.write() = true;
                imp.tag_list.set_selection(id);
            }
            SidebarIterateItem::TagListSelectFirstItem => {
                // if tags not supported or not available jump back to "All Articles"
                if !imp.tags_box.is_visible() {
                    return self.select_item(SidebarIterateItem::SelectAll);
                }
                Self::expand_list(&imp.tags_revealer, &imp.tags_expander);
                if let Some(item) = imp.tag_list.get_first_item() {
                    *imp.delay_next_taglist_activation.write() = true;
                    imp.tag_list.set_selection(item);
                }
            }
            SidebarIterateItem::TagListSelectLastItem => {
                // if tags not supported or not available jump back to "All Articles"
                if !imp.tags_box.is_visible() {
                    return self.select_item(SidebarIterateItem::FeedListSelectLastItem);
                }
                Self::expand_list(&imp.tags_revealer, &imp.tags_expander);
                if let Some(item) = imp.tag_list.get_last_item() {
                    *imp.delay_next_taglist_activation.write() = true;
                    imp.tag_list.set_selection(item);
                }
            }
            SidebarIterateItem::NothingSelected => { /* nothing */ }
        }
    }

    pub fn get_selection(&self) -> SidebarSelection {
        let imp = imp::SideBar::from_instance(self);
        (*imp.selection.read()).clone()
    }

    fn deselect(&self) {
        let imp = imp::SideBar::from_instance(self);

        GtkUtil::remove_source(imp.delayed_all_selection.write().take());
        GtkUtil::remove_source(imp.delayed_feedlist_selection.write().take());
        GtkUtil::remove_source(imp.delayed_taglist_selection.write().take());
        imp.all_articles_fake_list.unselect_all();
        imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);
        imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);
    }

    pub fn expand_collapse_selected_category(&self) {
        let imp = imp::SideBar::from_instance(self);
        imp.feed_list.expand_collapse_selected_category()
    }
}
