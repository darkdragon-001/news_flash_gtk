mod search_item_row;

use self::search_item_row::SearchItemRow;
use crate::app::App;
use crate::util::{Util, CHANNEL_ERROR, RUNTIME_ERROR};
use feedly_api::{models::SearchResult, ApiError, FeedlyApi};
use futures::channel::oneshot;
use futures::FutureExt;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, ComboBox, CompositeTemplate, ListBox, SearchEntry, Stack, Widget, Window};
use libadwaita::{subclass::prelude::AdwWindowImpl, Window as AdwWindow};
use parking_lot::RwLock;
use std::sync::Arc;
use tokio::runtime::Runtime;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/discover_dialog.ui")]
    pub struct DiscoverDialog {
        pub current_query: Arc<RwLock<Option<String>>>,

        #[template_child]
        pub search_entry: TemplateChild<SearchEntry>,
        #[template_child]
        pub language_combo: TemplateChild<ComboBox>,
        #[template_child]
        pub search_page_stack: TemplateChild<Stack>,
        #[template_child]
        pub search_result_stack: TemplateChild<Stack>,
        #[template_child]
        pub search_result_list: TemplateChild<ListBox>,
        #[template_child]
        pub news_card_button: TemplateChild<Button>,
        #[template_child]
        pub tech_card_button: TemplateChild<Button>,
        #[template_child]
        pub science_card_button: TemplateChild<Button>,
        #[template_child]
        pub culture_card_button: TemplateChild<Button>,
        #[template_child]
        pub media_card_button: TemplateChild<Button>,
        #[template_child]
        pub sports_card_button: TemplateChild<Button>,
        #[template_child]
        pub food_card_button: TemplateChild<Button>,
        #[template_child]
        pub foss_card_button: TemplateChild<Button>,
    }

    impl Default for DiscoverDialog {
        fn default() -> Self {
            DiscoverDialog {
                current_query: Arc::new(RwLock::new(None)),

                search_entry: TemplateChild::default(),
                language_combo: TemplateChild::default(),
                search_page_stack: TemplateChild::default(),
                search_result_stack: TemplateChild::default(),
                search_result_list: TemplateChild::default(),
                news_card_button: TemplateChild::default(),
                tech_card_button: TemplateChild::default(),
                science_card_button: TemplateChild::default(),
                culture_card_button: TemplateChild::default(),
                media_card_button: TemplateChild::default(),
                sports_card_button: TemplateChild::default(),
                food_card_button: TemplateChild::default(),
                foss_card_button: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DiscoverDialog {
        const NAME: &'static str = "DiscoverDialog";
        type ParentType = AdwWindow;
        type Type = super::DiscoverDialog;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DiscoverDialog {}

    impl WidgetImpl for DiscoverDialog {}

    impl WindowImpl for DiscoverDialog {}

    impl AdwWindowImpl for DiscoverDialog {}
}

glib::wrapper! {
    pub struct DiscoverDialog(ObjectSubclass<imp::DiscoverDialog>)
        @extends Widget, gtk4::Window, AdwWindow;
}

impl DiscoverDialog {
    pub fn new<W: IsA<Window> + GtkWindowExt>(parent: &W) -> Self {
        let dialog = glib::Object::new::<Self>(&[]).unwrap();
        dialog.set_transient_for(Some(parent));

        let imp = imp::DiscoverDialog::from_instance(&dialog);

        dialog.setup_featured(imp.news_card_button.get(), "news");
        dialog.setup_featured(imp.tech_card_button.get(), "tech");
        dialog.setup_featured(imp.science_card_button.get(), "science");
        dialog.setup_featured(imp.culture_card_button.get(), "culture");
        dialog.setup_featured(imp.media_card_button.get(), "media");
        dialog.setup_featured(imp.sports_card_button.get(), "sports");
        dialog.setup_featured(imp.food_card_button.get(), "food");
        dialog.setup_featured(imp.foss_card_button.get(), "open source");

        imp.search_entry.connect_search_changed(clone!(
            @strong imp.current_query as current_query,
            @weak dialog => @default-panic, move |search_entry|
        {
            let imp = imp::DiscoverDialog::from_instance(&dialog);

            let query = search_entry.text();
            let query = query.as_str();
            let locale = imp.language_combo.active_id().map(|id| id.as_str().to_owned());

            if query.trim() != "" {
                dialog.feedly_search(locale);
            } else {
                dialog.clear_list();
                imp.search_page_stack.set_visible_child_name("featured");
            }
        }));

        imp.language_combo.set_active(Some(0));
        imp.language_combo.connect_changed(clone!(
            @strong imp.current_query as current_query,
            @weak dialog => @default-panic, move |language_combo|
        {
            let imp = imp::DiscoverDialog::from_instance(&dialog);

            let query = imp.search_entry.text();
            let query = query.as_str();
            let locale = language_combo.active_id().map(|id| id.as_str().to_owned());

            if query.trim() != "" {
                dialog.feedly_search(locale);
            } else {
                dialog.clear_list();
                imp.search_page_stack.set_visible_child_name("featured");
            }
        }));

        dialog
    }

    fn feedly_search(&self, locale: Option<String>) {
        let imp = imp::DiscoverDialog::from_instance(&self);

        let query = imp.search_entry.text().as_str().to_owned();
        imp.current_query.write().replace(query.clone());
        imp.search_page_stack.set_visible_child_name("search");
        imp.search_result_stack.set_visible_child_name("spinner");
        self.clear_list();
        let count = Some(30);

        let (sender, receiver) = oneshot::channel::<(String, Result<SearchResult, ApiError>)>();

        let thread_future = async move {
            let result = Runtime::new()
                .expect(RUNTIME_ERROR)
                .block_on(FeedlyApi::search_feedly_cloud(
                    &Util::build_client(),
                    &query,
                    count,
                    locale.as_deref(),
                ));
            sender.send((query, result)).expect(CHANNEL_ERROR);
        };

        let glib_future = receiver.map(clone!(
            @weak self as dialog => @default-panic, move |res|
        {
            let imp = imp::DiscoverDialog::from_instance(&dialog);

            if let Ok(res) = res {
                let (query, search_result) = res;

                if Some(query) == *imp.current_query.read() {
                    match search_result {
                        Ok(search_result) => {
                            dialog.clear_list();

                            let result_count = search_result.results.len();
                            for search_item in search_result.results.iter() {
                                if search_item.title.is_none() {
                                    // dont show items without title
                                    continue;
                                }
                                let search_item_row = SearchItemRow::new(&search_item);
                                imp.search_result_list.insert(&search_item_row, -1);
                            }

                            if result_count > 0 {
                                imp.search_result_stack.set_visible_child_name("list");
                            } else {
                                imp.search_result_stack.set_visible_child_name("empty");
                            }
                        },
                        Err(e) => {
                            imp.search_result_stack.set_visible_child_name("list");
                            log::error!("Feedly search query failed: '{}'", e);
                        },
                    }
                    imp.current_query.write().take();
                }
            } else {
                log::error!("Failed to receive search result!");
            }
        }));

        App::default().threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn clear_list(&self) {
        let imp = imp::DiscoverDialog::from_instance(&self);

        while let Some(row) = imp.search_result_list.first_child() {
            imp.search_result_list.remove(&row);
        }
    }

    fn setup_featured(&self, button: Button, topic_name: &str) {
        let imp = imp::DiscoverDialog::from_instance(&self);

        let topic_name_string = topic_name.to_owned();
        let search_entry = imp.search_entry.get();
        button.connect_clicked(move |_button| {
            search_entry.set_text(&format!("#{}", topic_name_string));
        });
    }
}
