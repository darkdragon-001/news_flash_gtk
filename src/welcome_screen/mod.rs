mod service_row;

use self::service_row::ServiceRow;
use crate::app::Action;
use crate::login_screen::{PasswordLoginPrevPage, WebLoginPrevPage};
use crate::util::Util;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, ListBox};
use news_flash::models::{LoginData, LoginGUI, PluginID};
use news_flash::NewsFlash;
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/welcome_page.ui")]
    pub struct WelcomePage {
        pub services: Arc<RwLock<HashMap<i32, (PluginID, LoginGUI)>>>,

        #[template_child]
        pub list: TemplateChild<ListBox>,
    }

    impl Default for WelcomePage {
        fn default() -> Self {
            Self {
                services: Arc::new(RwLock::new(HashMap::new())),
                list: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WelcomePage {
        const NAME: &'static str = "WelcomePage";
        type ParentType = gtk4::Box;
        type Type = super::WelcomePage;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WelcomePage {}

    impl WidgetImpl for WelcomePage {}

    impl BoxImpl for WelcomePage {}
}

glib::wrapper! {
    pub struct WelcomePage(ObjectSubclass<imp::WelcomePage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl WelcomePage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }
    pub fn init(&self) {
        self.populate();
        self.connect_signals();
    }

    fn populate(&self) {
        let services = NewsFlash::list_backends();
        let imp = imp::WelcomePage::from_instance(self);

        for (index, (id, api_meta)) in services.iter().enumerate() {
            let row = ServiceRow::new();
            row.init(&api_meta);
            imp.list.insert(&row, index as i32);
            imp.services
                .write()
                .insert(index as i32, (id.clone(), api_meta.login_gui.clone()));
        }
    }

    fn connect_signals(&self) {
        let imp = imp::WelcomePage::from_instance(self);

        imp.list.connect_row_activated(
            clone!(@strong imp.services as services => @default-panic, move |_list, row| {
                if let Some((id, login_desc)) = services.read().get(&row.index()) {
                    match login_desc {
                        LoginGUI::OAuth(_) => {
                            Util::send(Action::ShowOauthLogin(id.clone(), WebLoginPrevPage::Welcome));
                        }
                        LoginGUI::Password(_) => {
                            Util::send(Action::ShowPasswordLogin(id.clone(), None, PasswordLoginPrevPage::Welcome));
                        }
                        LoginGUI::None => {
                            Util::send(Action::Login(LoginData::None(id.clone())));
                        }
                    };
                }
            }),
        );
    }
}
