use super::SettingsDialog;
use crate::app::{Action, App};
use crate::settings::dialog::keybinding_editor::{KeybindState, KeybindingEditor};
use crate::settings::keybindings::Keybindings;
use crate::util::Util;
use glib::{clone, object::Cast};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Label, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, ActionRow, PreferencesPage};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/settings_shortcuts.ui")]
    pub struct SettingsShortcutsPage {
        #[template_child]
        pub next_article_row: TemplateChild<ActionRow>,
        #[template_child]
        pub next_article_label: TemplateChild<Label>,

        #[template_child]
        pub previous_article_row: TemplateChild<ActionRow>,
        #[template_child]
        pub previous_article_label: TemplateChild<Label>,

        #[template_child]
        pub toggle_read_row: TemplateChild<ActionRow>,
        #[template_child]
        pub toggle_read_label: TemplateChild<Label>,

        #[template_child]
        pub toggle_marked_row: TemplateChild<ActionRow>,
        #[template_child]
        pub toggle_marked_label: TemplateChild<Label>,

        #[template_child]
        pub open_browser_row: TemplateChild<ActionRow>,
        #[template_child]
        pub open_browser_label: TemplateChild<Label>,

        #[template_child]
        pub next_item_row: TemplateChild<ActionRow>,
        #[template_child]
        pub next_item_label: TemplateChild<Label>,

        #[template_child]
        pub previous_item_row: TemplateChild<ActionRow>,
        #[template_child]
        pub previous_item_label: TemplateChild<Label>,

        #[template_child]
        pub toggle_category_expanded_row: TemplateChild<ActionRow>,
        #[template_child]
        pub toggle_category_expanded_label: TemplateChild<Label>,

        #[template_child]
        pub sidebar_set_read_row: TemplateChild<ActionRow>,
        #[template_child]
        pub sidebar_set_read_label: TemplateChild<Label>,

        #[template_child]
        pub shortcuts_row: TemplateChild<ActionRow>,
        #[template_child]
        pub shortcuts_label: TemplateChild<Label>,

        #[template_child]
        pub scroll_up_row: TemplateChild<ActionRow>,
        #[template_child]
        pub scroll_up_label: TemplateChild<Label>,

        #[template_child]
        pub scroll_down_row: TemplateChild<ActionRow>,
        #[template_child]
        pub scroll_down_label: TemplateChild<Label>,

        #[template_child]
        pub scrap_content_row: TemplateChild<ActionRow>,
        #[template_child]
        pub scrap_content_label: TemplateChild<Label>,

        #[template_child]
        pub tag_row: TemplateChild<ActionRow>,
        #[template_child]
        pub tag_label: TemplateChild<Label>,

        #[template_child]
        pub search_row: TemplateChild<ActionRow>,
        #[template_child]
        pub search_label: TemplateChild<Label>,

        #[template_child]
        pub refresh_row: TemplateChild<ActionRow>,
        #[template_child]
        pub refresh_label: TemplateChild<Label>,

        #[template_child]
        pub quit_row: TemplateChild<ActionRow>,
        #[template_child]
        pub quit_label: TemplateChild<Label>,

        #[template_child]
        pub all_articles_row: TemplateChild<ActionRow>,
        #[template_child]
        pub all_articles_label: TemplateChild<Label>,

        #[template_child]
        pub only_unread_row: TemplateChild<ActionRow>,
        #[template_child]
        pub only_unread_label: TemplateChild<Label>,

        #[template_child]
        pub only_starred_row: TemplateChild<ActionRow>,
        #[template_child]
        pub only_starred_label: TemplateChild<Label>,
    }

    impl Default for SettingsShortcutsPage {
        fn default() -> Self {
            Self {
                next_article_row: TemplateChild::default(),
                next_article_label: TemplateChild::default(),

                previous_article_row: TemplateChild::default(),
                previous_article_label: TemplateChild::default(),

                toggle_read_row: TemplateChild::default(),
                toggle_read_label: TemplateChild::default(),

                toggle_marked_row: TemplateChild::default(),
                toggle_marked_label: TemplateChild::default(),

                open_browser_row: TemplateChild::default(),
                open_browser_label: TemplateChild::default(),

                next_item_row: TemplateChild::default(),
                next_item_label: TemplateChild::default(),

                previous_item_row: TemplateChild::default(),
                previous_item_label: TemplateChild::default(),

                toggle_category_expanded_row: TemplateChild::default(),
                toggle_category_expanded_label: TemplateChild::default(),

                sidebar_set_read_row: TemplateChild::default(),
                sidebar_set_read_label: TemplateChild::default(),

                shortcuts_row: TemplateChild::default(),
                shortcuts_label: TemplateChild::default(),

                scroll_up_row: TemplateChild::default(),
                scroll_up_label: TemplateChild::default(),

                scroll_down_row: TemplateChild::default(),
                scroll_down_label: TemplateChild::default(),

                scrap_content_row: TemplateChild::default(),
                scrap_content_label: TemplateChild::default(),

                tag_row: TemplateChild::default(),
                tag_label: TemplateChild::default(),

                refresh_row: TemplateChild::default(),
                refresh_label: TemplateChild::default(),

                search_row: TemplateChild::default(),
                search_label: TemplateChild::default(),

                quit_row: TemplateChild::default(),
                quit_label: TemplateChild::default(),

                all_articles_row: TemplateChild::default(),
                all_articles_label: TemplateChild::default(),

                only_unread_row: TemplateChild::default(),
                only_unread_label: TemplateChild::default(),

                only_starred_row: TemplateChild::default(),
                only_starred_label: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsShortcutsPage {
        const NAME: &'static str = "SettingsShortcutsPage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsShortcutsPage;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsShortcutsPage {}

    impl WidgetImpl for SettingsShortcutsPage {}

    impl PreferencesPageImpl for SettingsShortcutsPage {}
}

glib::wrapper! {
    pub struct SettingsShortcutsPage(ObjectSubclass<imp::SettingsShortcutsPage>)
        @extends Widget, PreferencesPage;
}

impl SettingsShortcutsPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self, parent: &SettingsDialog) {
        self.setup_interaction(parent);
    }

    fn setup_interaction(&self, parent: &SettingsDialog) {
        let imp = imp::SettingsShortcutsPage::from_instance(self);
        let settings = App::default().settings();

        self.setup_keybinding_row(
            &imp.next_article_row,
            &imp.next_article_label,
            "next_article",
            settings.read().get_keybind_article_list_next(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.previous_article_row,
            &imp.previous_article_label,
            "previous_article",
            settings.read().get_keybind_article_list_prev(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.toggle_read_row,
            &imp.toggle_read_label,
            "toggle_read",
            settings.read().get_keybind_article_list_read(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.toggle_marked_row,
            &imp.toggle_marked_label,
            "toggle_marked",
            settings.read().get_keybind_article_list_mark(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.open_browser_row,
            &imp.open_browser_label,
            "open_browser",
            settings.read().get_keybind_article_list_open(),
            parent,
        );

        self.setup_keybinding_row(
            &imp.next_item_row,
            &imp.next_item_label,
            "next_item",
            settings.read().get_keybind_feed_list_next(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.previous_item_row,
            &imp.previous_item_label,
            "previous_item",
            settings.read().get_keybind_feed_list_prev(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.toggle_category_expanded_row,
            &imp.toggle_category_expanded_label,
            "toggle_category_expanded",
            settings.read().get_keybind_feed_list_toggle_expanded(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.sidebar_set_read_row,
            &imp.sidebar_set_read_label,
            "sidebar_set_read",
            settings.read().get_keybind_sidebar_set_read(),
            parent,
        );

        self.setup_keybinding_row(
            &imp.shortcuts_row,
            &imp.shortcuts_label,
            "shortcuts",
            settings.read().get_keybind_shortcut(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.refresh_row,
            &imp.refresh_label,
            "refresh",
            settings.read().get_keybind_refresh(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.search_row,
            &imp.search_label,
            "search",
            settings.read().get_keybind_search(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.quit_row,
            &imp.quit_label,
            "quit",
            settings.read().get_keybind_quit(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.all_articles_row,
            &imp.all_articles_label,
            "all_articles",
            settings.read().get_keybind_all_articles(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.only_unread_row,
            &imp.only_unread_label,
            "only_unread",
            settings.read().get_keybind_only_unread(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.only_starred_row,
            &imp.only_starred_label,
            "only_starred",
            settings.read().get_keybind_only_starred(),
            parent,
        );

        self.setup_keybinding_row(
            &imp.scroll_up_row,
            &imp.scroll_up_label,
            "scroll_up",
            settings.read().get_keybind_article_view_up(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.scroll_down_row,
            &imp.scroll_down_label,
            "scroll_down",
            settings.read().get_keybind_article_view_down(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.scrap_content_row,
            &imp.scrap_content_label,
            "scrap_content",
            settings.read().get_keybind_article_view_scrap(),
            parent,
        );
        self.setup_keybinding_row(
            &imp.tag_row,
            &imp.tag_label,
            "tag",
            settings.read().get_keybind_article_view_tag(),
            parent,
        );
    }

    fn setup_keybinding_row(
        &self,
        row: &ActionRow,
        label: &Label,
        id: &str,
        keybinding: Option<String>,
        parent: &SettingsDialog,
    ) {
        Self::keybind_label_text(keybinding, label);
        let row_name = row.widget_name().as_str().to_owned();
        let id = id.to_owned();

        let info_text = row.title();
        row.connect_activated(clone!(
            @weak parent as dialog,
            @weak label,
            @strong id => @default-panic, move |row|
        {
            if row.widget_name().as_str() == row_name {
                let editor = KeybindingEditor::new();
                editor.init(&info_text);
                editor.set_transient_for(Some(&dialog));
                editor.present();
                editor.connect_destroy(clone!(
                    @weak label,
                    @strong id => @default-panic, move |editor|
                {
                    let editor = editor.downcast_ref::<KeybindingEditor>().expect("Failed to cast KeybindingEditor");
                    match editor.keybinding() {
                        KeybindState::Canceled | KeybindState::Illegal => {}
                        KeybindState::Disabled => {
                            if Keybindings::write_keybinding(&id, None, &App::default().settings()).is_ok() {
                                Self::keybind_label_text(None, &label);
                            } else {
                                Util::send(
                                    Action::SimpleMessage("Failed to write keybinding.".to_owned()),
                                );
                            }
                        }
                        KeybindState::Enabled(keybind) => {
                            if Keybindings::write_keybinding(&id, Some(keybind.clone()), &App::default().settings()).is_ok()
                            {
                                Self::keybind_label_text(Some(keybind.clone()), &label);
                            } else {
                                Util::send(
                                    Action::SimpleMessage("Failed to write keybinding.".to_owned()),
                                );
                            }
                        }
                    }
                }));
            }
        }));
    }

    fn keybind_label_text(keybinding: Option<String>, label: &Label) {
        let label_text = match keybinding {
            Some(keybinding) => {
                label.set_sensitive(true);
                Keybindings::parse_shortcut_string(&keybinding)
                    .expect("Failed parsing saved shortcut. This should never happen!")
            }
            None => {
                label.set_sensitive(false);
                "Disabled".to_owned()
            }
        };
        label.set_label(&label_text);
    }
}
