use crate::settings::UserDataSize;
use crate::util::Util;
use crate::{
    app::{Action, App},
    settings::general::{KeepArticlesDuration, PredefinedSyncInterval, SyncInterval},
};
use chrono::Duration;
use futures::{channel::oneshot, FutureExt};
use glib::{clone, RustClosure};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, CheckButton, ClosureExpression, CompositeTemplate, Entry, Inhibit, Label, Switch, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, ComboRow, EnumListItem, EnumListModel, PreferencesPage};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/settings_app_page.ui")]
    pub struct SettingsAppPage {
        #[template_child]
        pub keep_running_switch: TemplateChild<Switch>,
        #[template_child]
        pub startup_sync_switch: TemplateChild<Switch>,
        #[template_child]
        pub metered_sync_switch: TemplateChild<Switch>,
        #[template_child]
        pub predefined_sync_row: TemplateChild<ComboRow>,
        #[template_child]
        pub custom_update_entry: TemplateChild<Entry>,
        #[template_child]
        pub manual_update_check: TemplateChild<CheckButton>,
        #[template_child]
        pub predefined_update_check: TemplateChild<CheckButton>,
        #[template_child]
        pub custom_update_check: TemplateChild<CheckButton>,
        #[template_child]
        pub clear_cache_button: TemplateChild<Button>,
        #[template_child]
        pub user_data_label: TemplateChild<Label>,
        #[template_child]
        pub cache_label: TemplateChild<Label>,
        #[template_child]
        pub limit_articles_row: TemplateChild<ComboRow>,
    }

    impl Default for SettingsAppPage {
        fn default() -> Self {
            Self {
                keep_running_switch: TemplateChild::default(),
                startup_sync_switch: TemplateChild::default(),
                metered_sync_switch: TemplateChild::default(),
                predefined_sync_row: TemplateChild::default(),
                custom_update_entry: TemplateChild::default(),
                manual_update_check: TemplateChild::default(),
                predefined_update_check: TemplateChild::default(),
                custom_update_check: TemplateChild::default(),
                clear_cache_button: TemplateChild::default(),
                user_data_label: TemplateChild::default(),
                cache_label: TemplateChild::default(),
                limit_articles_row: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsAppPage {
        const NAME: &'static str = "SettingsAppPage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsAppPage;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsAppPage {}

    impl WidgetImpl for SettingsAppPage {}

    impl PreferencesPageImpl for SettingsAppPage {}
}

glib::wrapper! {
    pub struct SettingsAppPage(ObjectSubclass<imp::SettingsAppPage>)
        @extends Widget, PreferencesPage;
}

impl SettingsAppPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let settings = App::default().settings();
        let imp = imp::SettingsAppPage::from_instance(self);

        imp.keep_running_switch
            .set_state(settings.read().get_keep_running_in_background());

        imp.startup_sync_switch.set_state(settings.read().get_sync_on_startup());
        imp.metered_sync_switch.set_state(settings.read().get_sync_on_metered());

        match settings.read().get_sync_interval() {
            SyncInterval::Never => {
                imp.manual_update_check.set_active(true);
                imp.custom_update_entry.set_sensitive(false);
            }
            SyncInterval::Predefined(_) => {
                imp.predefined_update_check.set_active(true);
                imp.custom_update_entry.set_sensitive(false);
            }
            SyncInterval::Custom(total_seconds) => {
                let hours = total_seconds / 3600;
                let minutes = (total_seconds - hours * 3600) / 60;
                let seconds = total_seconds - hours * 3600 - minutes * 60;

                imp.custom_update_entry
                    .set_text(&format!("{:02}:{:02}:{:02}", hours, minutes, seconds));
                imp.custom_update_check.set_active(true);
                imp.custom_update_entry.set_sensitive(true);
            }
        }

        let params: &[gtk4::Expression] = &[];
        let closure = RustClosure::new(|values| {
            let e = values[0].get::<EnumListItem>().unwrap();
            let sync = PredefinedSyncInterval::from_u32(e.value() as u32)
                .to_string()
                .to_value();
            return Some(sync);
        });
        let sync_interval_closure = ClosureExpression::new::<String, _, _>(params, closure);
        imp.predefined_sync_row.set_expression(Some(&sync_interval_closure));
        imp.predefined_sync_row
            .set_model(Some(&EnumListModel::new(PredefinedSyncInterval::static_type())));
        imp.predefined_sync_row.set_selected(
            if let SyncInterval::Predefined(predefined_interval) = settings.read().get_sync_interval() {
                predefined_interval.to_u32()
            } else {
                0
            },
        );

        let closure = RustClosure::new(|values| {
            let e = values[0].get::<EnumListItem>().unwrap();
            let keep = KeepArticlesDuration::from_u32(e.value() as u32).to_string().to_value();
            return Some(keep);
        });
        let limit_articles_closure = ClosureExpression::new::<String, _, _>(params, closure);
        imp.limit_articles_row.set_expression(Some(&limit_articles_closure));
        imp.limit_articles_row
            .set_model(Some(&EnumListModel::new(KeepArticlesDuration::static_type())));

        if let Some(news_flash) = App::default().news_flash().read().as_ref() {
            imp.limit_articles_row
                .set_selected(KeepArticlesDuration::from_duration(news_flash.get_keep_articles_duration()).to_u32());
        }

        self.setup_data_section();
        self.setup_interaction();
    }

    fn setup_interaction(&self) {
        let imp = imp::SettingsAppPage::from_instance(self);

        imp.keep_running_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_keep_running_in_background(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'keep running'.".to_owned(),
                ));
            }
            Inhibit(false)
        });

        imp.startup_sync_switch.connect_state_set(|_switch, is_set| {
            if App::default().settings().write().set_sync_on_startup(is_set).is_err() {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'sync on startup'.".to_owned(),
                ));
            }
            Inhibit(false)
        });

        imp.metered_sync_switch.connect_state_set(|_switch, is_set| {
            if App::default().settings().write().set_sync_on_metered(is_set).is_err() {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'sync on metered'.".to_owned(),
                ));
            }
            Inhibit(false)
        });

        imp.manual_update_check
            .connect_toggled(clone!(@weak self as this => @default-panic, move |check| {
                if check.is_active() {
                    let imp = imp::SettingsAppPage::from_instance(&this);
                    imp.custom_update_entry.set_sensitive(false);
                    Self::set_sync_interval(SyncInterval::Never);
                }
            }));

        imp.predefined_update_check.connect_toggled(clone!(@weak self as this => @default-panic, move |check| {
            if check.is_active() {
                let imp = imp::SettingsAppPage::from_instance(&this);
                imp.custom_update_entry.set_sensitive(false);
                let sync_interval = SyncInterval::Predefined(PredefinedSyncInterval::from_u32(imp.predefined_sync_row.selected()));
                Self::set_sync_interval(sync_interval);
            }
        }));

        imp.custom_update_check
            .connect_toggled(clone!(@weak self as this => @default-panic, move |check| {
                if check.is_active() {
                    let imp = imp::SettingsAppPage::from_instance(&this);
                    imp.custom_update_entry.set_sensitive(true);
                    Self::parse_custom_sync_interval(&imp.custom_update_entry.get());
                }
            }));

        imp.predefined_sync_row
            .connect_selected_notify(clone!(@weak self as this => @default-panic, move |row| {
                let imp = imp::SettingsAppPage::from_instance(&this);
                if imp.predefined_update_check.is_active() {
                    let sync_interval = SyncInterval::Predefined(PredefinedSyncInterval::from_u32(row.selected()));
                    Self::set_sync_interval(sync_interval);
                }
            }));

        imp.custom_update_entry
            .connect_changed(clone!(@weak self as this => @default-panic, move |entry| {
                let imp = imp::SettingsAppPage::from_instance(&this);

                if imp.custom_update_check.is_active() {
                    Self::parse_custom_sync_interval(entry);
                }
            }));

        imp.limit_articles_row.connect_selected_notify(|row| {
            let limit_articles_duration = KeepArticlesDuration::from_u32(row.selected());
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if news_flash
                    .set_keep_articles_duration(limit_articles_duration.to_duration())
                    .is_err()
                {
                    Util::send(Action::SimpleMessage(
                        "Failed to set setting 'limit articles duration'.".to_owned(),
                    ));
                }
            }
        });

        let user_data_label = imp.user_data_label.get();
        let cache_label = imp.cache_label.get();
        imp.clear_cache_button.connect_clicked(clone!(@weak user_data_label,
            @weak cache_label => @default-panic, move |button| {

            button.set_sensitive(false);
            let (oneshot_sender, receiver) = oneshot::channel::<()>();
            Util::send(Action::ClearCache(oneshot_sender));

            let glib_future = receiver.map(clone!(
                @weak user_data_label,
                @weak cache_label,
                @weak button => @default-panic, move |res| match res {
                Ok(()) => {
                    Self::query_data_sizes(&user_data_label, &cache_label);
                    button.set_sensitive(true);
                },
                Err(_) => {},
            }));
            Util::glib_spawn_future(glib_future);
        }));
    }

    fn setup_data_section(&self) {
        let imp = imp::SettingsAppPage::from_instance(self);
        Self::query_data_sizes(&imp.user_data_label, &imp.cache_label);
    }

    fn query_data_sizes(user_data_label: &Label, cache_label: &Label) {
        let (oneshot_sender, receiver) = oneshot::channel::<UserDataSize>();
        Util::send(Action::QueryDiskSpace(oneshot_sender));
        let glib_future = receiver.map(clone!(
            @weak user_data_label,
            @weak cache_label  => @default-panic, move |res| match res {
            Ok(user_data_size) => {
                user_data_label.set_text(&Util::format_data_size(user_data_size.database.on_disk));
                cache_label.set_text(&Util::format_data_size(user_data_size.webkit));
            }
            Err(_) => {
                let msg = "Failed to query";
                user_data_label.set_text(msg);
                log::warn!("Receiving db size failed.");
            },
        }));
        Util::glib_spawn_future(glib_future);
    }

    fn parse_custom_sync_interval(entry: &Entry) {
        let text = entry.text();
        let text = text.as_str();
        let char_count = text.chars().count();

        if char_count == 0 {
            entry.style_context().remove_class("error");
            return;
        } else if char_count != 8 {
            entry.style_context().add_class("error");
            return;
        }

        let text_pieces: Vec<&str> = text.split(':').collect();
        if text_pieces.len() != 3 {
            entry.style_context().add_class("error");
            return;
        }

        let hours = text_pieces[0].parse::<u32>().ok();
        let minutes = text_pieces[1].parse::<u32>().ok();
        let seconds = text_pieces[2].parse::<u32>().ok();

        if hours.is_none() || minutes.is_none() || seconds.is_none() {
            entry.style_context().add_class("error");
            return;
        }

        let hours = hours.unwrap();
        let minutes = minutes.unwrap();
        let seconds = seconds.unwrap();

        let duration =
            Duration::hours(hours as i64) + Duration::minutes(minutes as i64) + Duration::seconds(seconds as i64);
        let total_seconds = duration.num_seconds() as u32;

        if total_seconds == 0 {
            entry.style_context().add_class("error");
            return;
        }

        entry.style_context().remove_class("error");
        Self::set_sync_interval(SyncInterval::Custom(total_seconds));
    }

    fn set_sync_interval(sync_interval: SyncInterval) {
        if App::default()
            .settings()
            .write()
            .set_sync_interval(sync_interval)
            .is_ok()
        {
            Util::send(Action::ScheduleSync);
        } else {
            Util::send(Action::SimpleMessage(
                "Failed to set setting 'sync interval'.".to_owned(),
            ));
        }
    }
}
