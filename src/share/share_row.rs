use glib::subclass;
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, GestureClick, Image, Label, ListBoxRow, Widget};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/share_row.ui")]
    pub struct ShareRow {
        #[template_child]
        pub logo: TemplateChild<Image>,
        #[template_child]
        pub title: TemplateChild<Label>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
    }

    impl Default for ShareRow {
        fn default() -> Self {
            Self {
                logo: TemplateChild::default(),
                title: TemplateChild::default(),
                row_click: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ShareRow {
        const NAME: &'static str = "ShareRow";
        type ParentType = ListBoxRow;
        type Type = super::ShareRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ShareRow {}

    impl WidgetImpl for ShareRow {}

    impl ListBoxRowImpl for ShareRow {}
}

glib::wrapper! {
    pub struct ShareRow(ObjectSubclass<imp::ShareRow>)
        @extends Widget, ListBoxRow;
}

impl ShareRow {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self, icon_name: &str, name: &str) {
        let imp = imp::ShareRow::from_instance(self);

        imp.title.set_text(name);
        imp.logo.set_icon_name(Some(&format!("{}-symbolic", icon_name)));
    }

    pub fn click(&self) -> &GestureClick {
        let imp = imp::ShareRow::from_instance(self);
        &imp.row_click
    }
}
