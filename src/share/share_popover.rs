use super::{
    share_row::ShareRow,
    share_service_info::{ShareServiceInfo, INSTAPAPER, MASTODON, POCKET, REDDIT, TELEGRAM, TWITTER},
};
use crate::{
    app::{Action, App},
    util::Util,
};
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, ListBox, Popover, Widget};
use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/share_popover.ui")]
    pub struct SharePopover {
        #[template_child]
        pub list: TemplateChild<ListBox>,
    }

    impl Default for SharePopover {
        fn default() -> Self {
            Self {
                list: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SharePopover {
        const NAME: &'static str = "SharePopover";
        type ParentType = Popover;
        type Type = super::SharePopover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SharePopover {}

    impl WidgetImpl for SharePopover {}

    impl PopoverImpl for SharePopover {}
}

glib::wrapper! {
    pub struct SharePopover(ObjectSubclass<imp::SharePopover>)
        @extends Widget, Popover;
}

impl SharePopover {
    pub fn new() -> Self {
        let this = glib::Object::new::<Self>(&[]).unwrap();
        this.update();
        this
    }

    pub fn update(&self) {
        self.clear();

        if App::default().settings().read().get_share_pocket_enabled() {
            self.insert_row(&POCKET);
        }

        if App::default().settings().read().get_share_instapaper_enabled() {
            self.insert_row(&INSTAPAPER);
        }

        if App::default().settings().read().get_share_twitter_enabled() {
            self.insert_row(&TWITTER);
        }

        if App::default().settings().read().get_share_mastodon_enabled() {
            self.insert_row(&MASTODON);
        }

        if App::default().settings().read().get_share_reddit_enabled() {
            self.insert_row(&REDDIT);
        }

        if App::default().settings().read().get_share_telegram_enabled() {
            self.insert_row(&TELEGRAM);
        }

        if App::default().settings().read().get_share_custom_enabled() {
            let name = App::default()
                .settings()
                .read()
                .get_share_custom_name()
                .unwrap_or("Custom")
                .to_owned();
            let url = App::default()
                .settings()
                .read()
                .get_share_custom_url()
                .unwrap_or("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
                .to_owned();
            self.insert_row_impl("emblem-shared-symbolic", &name, &url);
        }
    }

    fn clear(&self) {
        let imp = imp::SharePopover::from_instance(self);

        while let Some(row) = imp.list.first_child() {
            imp.list.remove(&row);
        }
    }

    fn insert_row(&self, info: &ShareServiceInfo) {
        self.insert_row_impl(info.icon_name, info.name, info.url_template);
    }

    fn insert_row_impl(&self, icon_name: &str, name: &str, url_template: &str) {
        let imp = imp::SharePopover::from_instance(self);

        let row = ShareRow::new();
        row.init(icon_name, name);

        let name = name.to_owned();
        let url_template = url_template.to_owned();
        row.click().connect_released(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y| {

            if times != 1 {
                return
            }

            let (article, _enclosures) = App::default().main_window().content_page().articleview_column().article_view().get_visible_article();
            let article = if let Some(article) = article {
                article
            } else {
                Util::send(Action::SimpleMessage("No article selected".into()));
                return;
            };

            let article_url = if let Some(url) = article.url {
                url
            } else {
                Util::send(Action::SimpleMessage("Article does not have URL".into()));
                return;
            };

            let title = article.title.unwrap_or("Unkown Article".into());

            let share_url = Self::prepare_share_url(&url_template, article_url.as_str(), &title);
            Util::send(Action::OpenUrlInDefaultBrowser(share_url));
            Util::send(Action::SimpleMessage(format!("Article shared with {}", name)));
            this.popdown();
        }));

        imp.list.append(&row);
    }

    fn prepare_share_url(url_template: &str, article_url: &str, title: &str) -> String {
        let article_url_escaped = utf8_percent_encode(article_url, NON_ALPHANUMERIC).to_string();
        let title_escaped = utf8_percent_encode(title, NON_ALPHANUMERIC).to_string();

        let url = url_template.replace("${url}", &article_url_escaped);
        let url = url.replace("${title}", &title_escaped);
        url
    }
}
