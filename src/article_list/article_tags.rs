use glib::clone;
use gtk4::{
    prelude::*, subclass::prelude::*, Align, Box, CompositeTemplate, CssProvider, Label,
    STYLE_PROVIDER_PRIORITY_APPLICATION,
};
use news_flash::models::Tag;

use crate::{
    color::ColorRGBA,
    util::{constants, GtkUtil},
};

mod imp {
    use gtk4::DrawingArea;

    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_tags.ui")]
    pub struct ArticleTags {
        #[template_child]
        pub full_names: TemplateChild<Box>,
        #[template_child]
        pub color_circles: TemplateChild<DrawingArea>,
    }

    impl Default for ArticleTags {
        fn default() -> Self {
            ArticleTags {
                full_names: TemplateChild::default(),
                color_circles: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleTags {
        const NAME: &'static str = "ArticleTags";
        type ParentType = gtk4::Box;
        type Type = super::ArticleTags;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleTags {}

    impl WidgetImpl for ArticleTags {}

    impl BoxImpl for ArticleTags {}
}

glib::wrapper! {
    pub struct ArticleTags(ObjectSubclass<imp::ArticleTags>)
        @extends gtk4::Widget, gtk4::Box;
}

impl ArticleTags {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn update_tags(&self, tags: &[Tag]) {
        self.create_labels(&tags);
        self.create_color_circles(&tags);
    }

    fn create_color_circles(&self, tags: &[Tag]) {
        let imp = imp::ArticleTags::from_instance(self);

        let colors = tags
            .iter()
            .map(|tag| tag.color.as_deref().unwrap_or(constants::TAG_DEFAULT_COLOR).to_owned())
            .collect::<Vec<_>>();

        imp.color_circles.set_draw_func(
            clone!(@strong colors => @default-panic, move |_drawing_area, ctx, width, height| {
                let circle_count = colors.len() as f64;
                let size = 16_f64;
                let margin = 2_f64;
                let half_size = size / 2_f64;
                let min_offset = half_size;
                let max_offset = size + margin;
                let offset = width as f64 / circle_count;
                let offset = offset.min(max_offset);
                let offset = offset.max(min_offset);

                for (i, color) in colors.iter().enumerate() {
                    let center_x = width as f64 - (half_size + 2.0 * margin) - (offset * i as f64);
                    let center_y = height as f64 / 2.0;
                    GtkUtil::draw_color_cirlce(&ctx, color, Some((center_x, center_y)));
                }
            }),
        );
        imp.color_circles.queue_draw();
    }

    fn create_labels(&self, tags: &[Tag]) {
        let imp = imp::ArticleTags::from_instance(self);

        while let Some(label) = imp.full_names.first_child() {
            imp.full_names.remove(&label);
        }

        for tag in tags {
            let label = Self::create_tag_label(&tag);
            imp.full_names.append(&label);
        }
    }

    fn create_tag_label(tag: &Tag) -> Label {
        let css_selector = tag
            .tag_id
            .as_str()
            .chars()
            .filter(|c| c.is_alphanumeric() && !c.is_whitespace())
            .collect::<String>();
        let label = Label::new(Some(&tag.label));
        label.set_margin_start(2);
        label.set_margin_end(2);
        label.set_valign(Align::Center);
        label.add_css_class(&css_selector);
        label.add_css_class("subtitle");
        let provider = CssProvider::new();
        let css_str = Self::generate_css(&css_selector, tag.color.as_deref());
        provider.load_from_data(&css_str);
        label
            .style_context()
            .add_provider(&provider, STYLE_PROVIDER_PRIORITY_APPLICATION);
        label
    }

    fn generate_css(tag_id: &str, color_str: Option<&str>) -> Vec<u8> {
        let tag_color = match ColorRGBA::parse_string(color_str.unwrap_or(constants::TAG_DEFAULT_COLOR)) {
            Ok(color) => color,
            Err(_) => ColorRGBA::parse_string(constants::TAG_DEFAULT_COLOR)
                .expect("Failed to parse default outer RGBA string."),
        };
        let gradient_upper = GtkUtil::adjust_lightness(&tag_color, constants::TAG_GRADIENT_SHIFT, None);
        let gradient_lower = GtkUtil::adjust_lightness(&tag_color, -constants::TAG_GRADIENT_SHIFT, None);

        let gradient_upper_hex = gradient_upper.to_string_no_alpha();
        let gradient_lower_hex = gradient_lower.to_string_no_alpha();

        let background_luminance = tag_color.luminance_normalized();
        let text_shift = if background_luminance > 0.8 {
            background_luminance - 1.3
        } else {
            1.2 - background_luminance
        };
        let mut text_color = tag_color.clone();
        text_color.adjust_lightness_absolute(text_shift).unwrap();
        text_color
            .adjust_saturation_absolute(constants::TAG_TEXT_SATURATION_SHIFT)
            .unwrap();
        let text_color_hex = text_color.to_string_no_alpha();

        let css_str = format!(
            ".{} {{
            background-image: linear-gradient({}, {});
            color: {};
            border-radius: 12px;
            min-width: 12px;
            padding: 2px 5px 1px;
            opacity: 1.0;
        }}",
            tag_id, gradient_upper_hex, gradient_lower_hex, text_color_hex
        );
        css_str.as_bytes().into()
    }
}
