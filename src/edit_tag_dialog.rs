use crate::{color::ColorRGBA, i18n::i18n, util::constants};
use gdk4::RGBA;
use glib::{clone, IsA};
use gtk4::{prelude::*, subclass::prelude::*, Button, ColorButton, CompositeTemplate, Entry, Widget, Window};
use libadwaita::{subclass::prelude::AdwWindowImpl, Window as AdwWindow};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/edit_tag_dialog.ui")]
    pub struct EditTagDialog {
        #[template_child]
        pub apply_button: TemplateChild<Button>,
        #[template_child]
        pub title_entry: TemplateChild<Entry>,
        #[template_child]
        pub color_button: TemplateChild<ColorButton>,
    }

    impl Default for EditTagDialog {
        fn default() -> Self {
            Self {
                apply_button: TemplateChild::default(),
                title_entry: TemplateChild::default(),
                color_button: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EditTagDialog {
        const NAME: &'static str = "EditTagDialog";
        type Type = super::EditTagDialog;
        type ParentType = AdwWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EditTagDialog {}

    impl WidgetImpl for EditTagDialog {}

    impl WindowImpl for EditTagDialog {}

    impl AdwWindowImpl for EditTagDialog {}
}

glib::wrapper! {
    pub struct EditTagDialog(ObjectSubclass<imp::EditTagDialog>)
        @extends Widget, Window;
}

impl EditTagDialog {
    pub fn new<W: IsA<Window>>(parent: &W, color: &Option<String>, title: &str) -> Self {
        let dialog = glib::Object::new(&[]).expect("Failed to create EditTagDialog");
        let imp = imp::EditTagDialog::from_instance(&dialog);

        imp.title_entry.set_text(title);

        let color = color.as_deref().unwrap_or(constants::TAG_DEFAULT_COLOR);
        if let Ok(rgba) = ColorRGBA::parse_string(color) {
            imp.color_button.set_rgba(&RGBA::new(
                rgba.red_normalized() as f32,
                rgba.green_normalized() as f32,
                rgba.blue_normalized() as f32,
                rgba.alpha_normalized() as f32,
            ));
        }

        imp.title_entry
            .connect_changed(clone!(@weak dialog as this => @default-panic, move |entry| {
                let imp = imp::EditTagDialog::from_instance(&this);

                if entry.text().as_str().is_empty() {
                    imp.apply_button.set_sensitive(false);
                    entry.style_context().add_class("warning");
                    entry.set_secondary_icon_name(Some("dialog-warning-symbolic"));
                    entry.set_secondary_icon_tooltip_text(Some(&i18n("Empty title not allowed")));
                } else {
                    imp.apply_button.set_sensitive(true);
                    entry.style_context().remove_class("warning");
                    entry.set_secondary_icon_name(None);
                    entry.set_secondary_icon_tooltip_text(None);
                }
            }));

        dialog.set_transient_for(Some(parent));
        dialog.present();
        dialog
    }

    pub fn text(&self) -> String {
        let imp = imp::EditTagDialog::from_instance(self);
        imp.title_entry.text().as_str().to_owned()
    }

    pub fn color(&self) -> ColorRGBA {
        let imp = imp::EditTagDialog::from_instance(self);
        let rgba = imp.color_button.rgba();
        ColorRGBA::from_normalized(
            rgba.red() as f64,
            rgba.green() as f64,
            rgba.blue() as f64,
            rgba.alpha() as f64,
        )
    }

    pub fn connect_edit<F: Fn(&Self) + 'static>(&self, f: F) {
        let imp = imp::EditTagDialog::from_instance(self);
        imp.apply_button
            .connect_clicked(clone!(@weak self as this => @default-panic, move |_button| {
                f(&this);
            }));
    }
}
