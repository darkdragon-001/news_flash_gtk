use crate::about_dialog::APP_NAME;
use crate::app::{Action, App};
use crate::config::{APP_ID, PROFILE};
use crate::content_page::ContentPage;
use crate::login_screen::{LoginPage, PasswordLoginPrevPage, WebLoginPrevPage};
use crate::reset_page::ResetPage;
use crate::settings::Keybindings;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::FeedListItemID;
use crate::undo_action::UndoAction;
use crate::util::{constants, Util, CHANNEL_ERROR, RUNTIME_ERROR};
use crate::welcome_screen::WelcomePage;
use futures::channel::oneshot;
use futures::FutureExt;
use gdk4::{Key, ModifierType};
use glib::{self, clone};
use gtk4::{self, EventControllerKey, Inhibit};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate};
use libadwaita::{subclass::prelude::*, Leaflet, LeafletTransitionType};
use log::error;
use news_flash::models::{
    ArticleID, Enclosure, FatArticle, Feed, PasswordLogin as PasswordLoginData, PluginCapabilities, PluginID,
};
use news_flash::{NewsFlash, NewsFlashError};
use parking_lot::RwLock;
use std::sync::Arc;
use std::time::Duration;
use tokio::runtime::Runtime;

const CONTENT_PAGE: &str = "content_page";
const WELCOME_PAGE: &str = "welcome_page";
const RESET_PAGE: &str = "reset_page";
const LOGIN_PAGE: &str = "login_page";

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/main_window.ui")]
    pub struct MainWindow {
        #[template_child]
        pub leaflet: TemplateChild<Leaflet>,
        #[template_child]
        pub shortcut_controller: TemplateChild<EventControllerKey>,

        #[template_child]
        pub welcome_page: TemplateChild<WelcomePage>,
        #[template_child]
        pub reset_page: TemplateChild<ResetPage>,
        #[template_child]
        pub login_page: TemplateChild<LoginPage>,
        #[template_child]
        pub content_page: TemplateChild<ContentPage>,
    }

    impl Default for MainWindow {
        fn default() -> Self {
            Self {
                leaflet: TemplateChild::default(),
                shortcut_controller: TemplateChild::default(),

                welcome_page: TemplateChild::default(),
                reset_page: TemplateChild::default(),
                login_page: TemplateChild::default(),

                content_page: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MainWindow {
        const NAME: &'static str = "MainWindow";
        type ParentType = libadwaita::ApplicationWindow;
        type Type = super::MainWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MainWindow {}

    impl WidgetImpl for MainWindow {}

    impl WindowImpl for MainWindow {}

    impl ApplicationWindowImpl for MainWindow {}

    impl AdwApplicationWindowImpl for MainWindow {}
}

glib::wrapper! {
    pub struct MainWindow(ObjectSubclass<imp::MainWindow>)
        @extends gtk4::Widget, gtk4::Window, gtk4::ApplicationWindow, libadwaita::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl MainWindow {
    pub fn new() -> Self {
        let window = glib::Object::new::<Self>(&[]).unwrap();
        window.set_icon_name(Some(APP_ID));
        window.set_title(Some(APP_NAME));
        if PROFILE == "Devel" {
            window.style_context().add_class("devel");
        }
        window
    }

    pub fn init(&self, shutdown_in_progress: &Arc<RwLock<bool>>) {
        let imp = imp::MainWindow::from_instance(self);

        imp.leaflet.connect_visible_child_name_notify(|deck| {
            deck.set_can_navigate_back(
                deck.visible_child_name().map(|s| s.as_str().to_owned()) != Some(CONTENT_PAGE.into()),
            );
        });

        libadwaita::StyleManager::default().connect_dark_notify(
            clone!(@weak self as window => @default-panic, move |_tm| {
                glib::timeout_add_local(Duration::from_millis(20), move || {
                        window.content_page().load_branding();
                        window.content_page().update_article_view_background();
                        Util::send(Action::RedrawArticle);
                        Continue(false)
                    }
                );
            }),
        );

        // setup pages
        imp.welcome_page.init();
        imp.login_page.init();
        imp.reset_page.init();
        imp.content_page.init();

        // setup shutdown
        let content_page = self.content_page();
        self.connect_close_request(clone!(
            @weak content_page,
            @strong shutdown_in_progress,
            @weak self as window => @default-panic, move |win|
        {
            let imp = imp::MainWindow::from_instance(&window);

            if *shutdown_in_progress.read() {
                win.hide();
                return Inhibit(true);
            }
            if App::default().settings().read().get_keep_running_in_background() {
                if let Some(visible_child) = imp.leaflet.visible_child_name() {
                    if visible_child == CONTENT_PAGE {
                        win.hide();
                    } else {
                        Util::send(Action::QueueQuit);
                    }
                }
            } else {
                Util::send(Action::QueueQuit);
            }
            Inhibit(true)
        }));

        let state = self.content_page().state();
        self.connect_fullscreened_notify(move |window| {
            let window_fullscreen = window.is_fullscreened();
            let video_fullscreen = state.read().get_video_fullscreen();

            if window_fullscreen {
                if video_fullscreen {
                    window.content_page().enter_fullscreen_video();
                } else {
                    window.content_page().enter_fullscreen_article();
                }
            } else {
                window.content_page().leave_fullscreen_video();
            }
        });

        self.setup_shortcuts();

        // set visible page
        imp.leaflet.set_visible_child_name(CONTENT_PAGE);
        if self.content_page().load_branding() {
            return;
        }

        // in case of failure show 'welcome page'
        imp.leaflet.set_visible_child_name(WELCOME_PAGE);
    }

    fn setup_shortcuts(&self) {
        let imp = imp::MainWindow::from_instance(self);
        let state = self.content_page().state();
        let leaflet = imp.leaflet.get();
        let content_page = self.content_page();

        imp.shortcut_controller.connect_key_pressed(clone!(
            @strong state,
            @weak leaflet,
            @weak content_page => @default-panic, move |_controller, key, _key_code, keyboard_state|
        {
            // ignore shortcuts when not on content page
            if let Some(visible_child) = leaflet.visible_child_name() {
                if visible_child != CONTENT_PAGE {
                    return Inhibit(false);
                }
            }

            // ignore shortcuts when typing in search entry
            if content_page.article_list_column().is_search_focused() {
                return Inhibit(false);
            }

            if Self::check_shortcut("shortcuts", &key, &keyboard_state) {
                Util::send(Action::ShowShortcutWindow);
                return Inhibit(true);
            }

            if Self::check_shortcut("refresh", &key, &keyboard_state) {
                if !state.read().get_offline() {
                    Util::send(Action::Sync);
                }
                return Inhibit(true);
            }

            if Self::check_shortcut("quit", &key, &keyboard_state) {
                Util::send(Action::QueueQuit);
                return Inhibit(true);
            }

            if Self::check_shortcut("search", &key, &keyboard_state) {
                content_page.article_list_column().focus_search();
                return Inhibit(true);
            }

            if Self::check_shortcut("all_articles", &key, &keyboard_state) {
                content_page.article_list_column().set_view_switcher_stack("all");
                return Inhibit(true);
            }

            if Self::check_shortcut("only_unread", &key, &keyboard_state) {
                content_page.article_list_column().set_view_switcher_stack("unread");
                return Inhibit(true);
            }

            if Self::check_shortcut("only_starred", &key, &keyboard_state) {
                content_page.article_list_column().set_view_switcher_stack("marked");
                return Inhibit(true);
            }

            if Self::check_shortcut("next_article", &key, &keyboard_state) {
                Util::send(Action::SelectNextArticle);
                return Inhibit(true);
            }

            if Self::check_shortcut("previous_article", &key, &keyboard_state) {
                Util::send(Action::SelectPrevArticle);
                return Inhibit(true);
            }

            if Self::check_shortcut("toggle_category_expanded", &key, &keyboard_state) {
                content_page.sidebar_column().sidebar().expand_collapse_selected_category();
                return Inhibit(true);
            }

            if Self::check_shortcut("toggle_read", &key, &keyboard_state) {
                if !state.read().get_offline() {
                    Util::send(Action::ToggleArticleRead);
                }
                return Inhibit(true);
            }

            if Self::check_shortcut("toggle_marked", &key, &keyboard_state) {
                if !state.read().get_offline() {
                    Util::send(Action::ToggleArticleMarked);
                }
                return Inhibit(true);
            }

            if Self::check_shortcut("open_browser", &key, &keyboard_state) {
                Util::send(Action::OpenSelectedArticle);
            }

            if Self::check_shortcut("next_item", &key, &keyboard_state){
                content_page.sidebar_select_next_item();
            }

            if Self::check_shortcut("previous_item", &key, &keyboard_state) {
                content_page.sidebar_select_prev_item();
            }

            if Self::check_shortcut("scroll_up", &key, &keyboard_state)
                && content_page.article_view_scroll_diff(-150.0).is_err()
            {
                Util::send(
                    Action::SimpleMessage("Failed to select scroll article view up.".to_owned()),
                );
                return Inhibit(true);
            }

            if Self::check_shortcut("scroll_down", &key, &keyboard_state)
                && content_page.article_view_scroll_diff(150.0).is_err()
            {
                Util::send(
                    Action::SimpleMessage("Failed to select scroll article view down.".to_owned()),
                );
                return Inhibit(true);
            }

            if Self::check_shortcut("scrap_content", &key, &keyboard_state) {
                Util::send(Action::StartGrabArticleContent);
                return Inhibit(true);
            }

            if Self::check_shortcut("tag", &key, &keyboard_state) {
                content_page.articleview_column().popup_tag_popover();
                return Inhibit(true);
            }

            if Self::check_shortcut("sidebar_set_read", &key, &keyboard_state) {
                if !state.read().get_offline() {
                    Util::send(Action::SetSidebarRead);
                }
                return Inhibit(true);
            }

            Inhibit(false)
        }));
    }

    fn check_shortcut(id: &str, pressed_key: &Key, state: &ModifierType) -> bool {
        if let Ok(keybinding) = Keybindings::read_keybinding(id) {
            if let Some(keybinding) = keybinding {
                if let Some((key, modifier)) = gtk4::accelerator_parse(&keybinding) {
                    if key.to_lower() == pressed_key.to_lower() {
                        if modifier.is_empty() {
                            if Keybindings::clean_modifier(*state).is_empty() {
                                return true;
                            }
                        } else if state.contains(modifier) {
                            return true;
                        }
                    }
                }
            }
        }
        false
    }

    pub fn content_page(&self) -> &ContentPage {
        let imp = imp::MainWindow::from_instance(self);
        &imp.content_page
    }

    pub fn login_page(&self) -> &LoginPage {
        let imp = imp::MainWindow::from_instance(self);
        &imp.login_page
    }

    pub fn show_undo_bar(&self, action: UndoAction) {
        let select_all_button = match self.content_page().sidebar_column().sidebar().get_selection() {
            SidebarSelection::All => false,
            SidebarSelection::FeedList(selected_id, _label) => match &action {
                UndoAction::DeleteCategory(delete_id, _label) => {
                    *selected_id == FeedListItemID::Category(delete_id.clone())
                }
                UndoAction::DeleteFeed(delete_id, _label) => {
                    if let FeedListItemID::Feed(feed_id, _) = &*selected_id {
                        feed_id == delete_id
                    } else {
                        false
                    }
                }
                _ => false,
            },

            SidebarSelection::Tag(selected_id, _label) => match &action {
                UndoAction::DeleteTag(delete_id, _label) => &*selected_id == delete_id,
                _ => false,
            },
        };
        if select_all_button {
            self.content_page()
                .state()
                .write()
                .set_sidebar_selection(SidebarSelection::All);
            self.content_page()
                .sidebar_column()
                .sidebar()
                .select_all_button_no_update();
        }

        self.content_page().add_undo_notification(action);
    }

    pub fn show_welcome_page(&self) {
        let imp = imp::MainWindow::from_instance(self);

        imp.login_page.reset();
        imp.leaflet.set_transition_type(LeafletTransitionType::Over);
        imp.leaflet.set_visible_child_name(WELCOME_PAGE);
    }

    pub fn show_password_login_page(
        &self,
        plugin_id: &PluginID,
        data: Option<PasswordLoginData>,
        prev_page: PasswordLoginPrevPage,
    ) {
        let imp = imp::MainWindow::from_instance(self);

        if let Some(service_meta) = NewsFlash::list_backends().get(plugin_id) {
            if let Ok(()) = imp
                .login_page
                .password_login()
                .set_service(service_meta.clone(), prev_page)
            {
                if let Some(data) = data {
                    imp.login_page.password_login().fill(data);
                }
                imp.leaflet.set_transition_type(LeafletTransitionType::Over);
                imp.leaflet.set_visible_child_name(LOGIN_PAGE);
                imp.login_page.set_visible_child_name("password_login")
            }
        }
    }

    pub fn show_oauth_login_page(&self, plugin_id: &PluginID, prev_page: WebLoginPrevPage) {
        let imp = imp::MainWindow::from_instance(self);

        if let Some(service_meta) = NewsFlash::list_backends().get(plugin_id) {
            if let Ok(()) = imp.login_page.web_login().set_service(service_meta.clone(), prev_page) {
                imp.leaflet.set_transition_type(LeafletTransitionType::Over);
                imp.leaflet.set_visible_child_name(LOGIN_PAGE);
                imp.login_page.set_visible_child_name("oauth_login")
            }
        }
    }

    pub fn show_content_page(&self) {
        let imp = imp::MainWindow::from_instance(self);

        if let Some(news_flash) = App::default().news_flash().read().as_ref() {
            imp.leaflet.set_transition_type(LeafletTransitionType::Over);
            imp.leaflet.set_visible_child_name(CONTENT_PAGE);

            Util::send(Action::UpdateSidebar);

            // show discover dialog if database is empty and backend supports adding feeds
            if App::default()
                .features()
                .read()
                .contains(PluginCapabilities::ADD_REMOVE_FEEDS)
            {
                if let Ok(true) = news_flash.is_database_empty() {
                    Util::send(Action::ShowDiscoverDialog);
                }
            }
        }

        self.content_page().load_branding();
    }

    pub fn show_reset_page(&self) {
        let imp = imp::MainWindow::from_instance(self);
        imp.reset_page.reset();
        imp.leaflet.set_transition_type(LeafletTransitionType::Over);
        imp.leaflet.set_visible_child_name(RESET_PAGE);
    }

    pub fn cancel_reset(&self) {
        let imp = imp::MainWindow::from_instance(self);
        imp.leaflet.set_transition_type(LeafletTransitionType::Over);
        imp.leaflet.set_visible_child_name(CONTENT_PAGE);
    }

    pub fn reset_account_failed(&self, error: NewsFlashError) {
        let imp = imp::MainWindow::from_instance(self);
        imp.reset_page.error(error);
    }

    pub fn sidebar_selection(&self, selection: SidebarSelection) {
        self.content_page().responsive_layout().show_sidebar();

        if &selection != self.content_page().state().write().get_sidebar_selection() {
            self.content_page().state().write().set_sidebar_selection(selection);
            Util::send(Action::UpdateArticleList);
        }
    }

    pub fn show_article(&self, article_id: Arc<ArticleID>) {
        let mut fat_article: Option<FatArticle> = None;
        let mut enclosures: Option<Vec<Enclosure>> = None;
        let mut feed_vec: Option<Vec<Feed>> = None;

        self.content_page().state().write().set_prefer_scraped_content(true);

        if let Some(news_flash) = App::default().news_flash().read().as_ref() {
            match news_flash.get_fat_article(&article_id) {
                Ok(article) => fat_article = Some(article),
                Err(error) => {
                    Util::send(Action::Error("Failed to read article.".to_owned(), error));
                    return;
                }
            };
            enclosures = news_flash
                .get_enclosures(&article_id)
                .map_err(|error| {
                    Util::send(Action::Error("Failed to read enclosures.".to_owned(), error));
                })
                .ok()
                .and_then(|enc| if enc.is_empty() { None } else { Some(enc) });
            match news_flash.get_feeds() {
                Ok((feeds, _mappings)) => feed_vec = Some(feeds),
                Err(error) => {
                    Util::send(Action::Error("Failed to read feeds.".to_owned(), error));
                    return;
                }
            };
        }

        if let Some(article) = fat_article {
            if let Some(feeds) = feed_vec {
                let feed = feeds.iter().find(|&f| f.feed_id == article.feed_id);
                self.content_page()
                    .articleview_column()
                    .show_article(Some(&article), enclosures.as_ref());
                self.content_page().articleview_column().article_view().show_article(
                    article,
                    feed.map(|f| f.label.clone()).unwrap_or(constants::UNKNOWN_FEED.into()),
                    enclosures,
                );

                self.content_page().responsive_layout().show_article_view();
            }
        }
    }

    pub fn set_search_term(&self, search_term: String) {
        if search_term.is_empty() {
            self.content_page().state().write().set_search_term(None);
        } else {
            self.content_page().state().write().set_search_term(Some(search_term));
        }

        Util::send(Action::UpdateArticleList);
    }

    pub fn set_sidebar_read(&self) {
        let sidebar_selection = self.content_page().state().read().get_sidebar_selection().clone();
        let content_page = self.content_page().clone();

        match sidebar_selection {
            SidebarSelection::All => {
                let (glib_sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

                let thread_future = async move {
                    let future = async move {
                        if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                            glib_sender
                                .send(news_flash.set_all_read(&Util::build_client()).await)
                                .expect(CHANNEL_ERROR);
                        }
                    };
                    Runtime::new().expect(RUNTIME_ERROR).block_on(future);
                };

                let glib_future = receiver.map(clone!(
                    @weak content_page => @default-panic, move |res|
                {
                    content_page.article_list_column().finish_mark_all_read();
                    res.map(|result| match result {
                        Ok(_) => {}
                        Err(error) => {
                            let message = "Failed to mark all read".to_owned();
                            error!("{}", message);
                            Util::send(Action::Error(message, error));
                        }
                    })
                    .expect(CHANNEL_ERROR);
                    Util::send(Action::UpdateArticleHeader);
                    Util::send(Action::UpdateArticleList);
                    Util::send(Action::UpdateSidebar);
                }));

                App::default().threadpool().spawn_ok(thread_future);
                Util::glib_spawn_future(glib_future);
            }
            SidebarSelection::FeedList(item_id, _title) => match &*item_id {
                FeedListItemID::Feed(feed_id, _parent_id) => {
                    let (glib_sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

                    let feed_id_vec = vec![feed_id.clone()];
                    let thread_future = async move {
                        if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                            glib_sender
                                .send(
                                    Runtime::new()
                                        .expect(RUNTIME_ERROR)
                                        .block_on(news_flash.set_feed_read(&feed_id_vec, &Util::build_client())),
                                )
                                .expect(CHANNEL_ERROR);
                        }
                    };

                    let glib_future = receiver.map(clone!(
                        @weak content_page => @default-panic, move |res|
                    {
                        content_page.article_list_column().finish_mark_all_read();
                        res.map(|result| match result {
                            Ok(_) => {}
                            Err(error) => {
                                let message = "Failed to mark all read".to_owned();
                                error!("{}", message);
                                Util::send(Action::Error(message, error));
                            }
                        })
                        .expect(CHANNEL_ERROR);
                        Util::send(Action::UpdateArticleHeader);
                        Util::send(Action::UpdateArticleList);
                        Util::send(Action::UpdateSidebar);
                    }));

                    App::default().threadpool().spawn_ok(thread_future);
                    Util::glib_spawn_future(glib_future);
                }
                FeedListItemID::Category(category_id) => {
                    let (glib_sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

                    let category_id_vec = vec![category_id.clone()];
                    let thread_future =
                        async move {
                            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                                glib_sender
                                    .send(Runtime::new().expect(RUNTIME_ERROR).block_on(
                                        news_flash.set_category_read(&category_id_vec, &Util::build_client()),
                                    ))
                                    .expect(CHANNEL_ERROR);
                            }
                        };

                    let glib_future = receiver.map(clone!(
                        @weak content_page => @default-panic, move |res|
                    {
                        content_page.article_list_column().finish_mark_all_read();
                        res.map(|result| match result {
                            Ok(_) => {}
                            Err(error) => {
                                let message = "Failed to mark all read".to_owned();
                                error!("{}", message);
                                Util::send(Action::Error(message, error));
                            }
                        })
                        .expect(CHANNEL_ERROR);
                        Util::send(Action::UpdateArticleHeader);
                        Util::send(Action::UpdateArticleList);
                        Util::send(Action::UpdateSidebar);
                    }));

                    App::default().threadpool().spawn_ok(thread_future);
                    Util::glib_spawn_future(glib_future);
                }
            },
            SidebarSelection::Tag(tag_id, _title) => {
                let (glib_sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

                let tag_id_vec = vec![(*tag_id).clone()];
                let thread_future = async move {
                    if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                        glib_sender
                            .send(
                                Runtime::new()
                                    .expect(RUNTIME_ERROR)
                                    .block_on(news_flash.set_tag_read(&tag_id_vec, &Util::build_client())),
                            )
                            .expect(CHANNEL_ERROR);
                    }
                };

                let glib_future = receiver.map(clone!(
                    @weak content_page => @default-panic, move |res|
                {
                    content_page.article_list_column().finish_mark_all_read();
                    res.map(|result| match result {
                        Ok(_) => {}
                        Err(error) => {
                            let message = "Failed to mark all read".to_owned();
                            error!("{}", message);
                            Util::send(Action::Error(message, error));
                        }
                    })
                    .expect(CHANNEL_ERROR);
                    Util::send(Action::UpdateArticleHeader);
                    Util::send(Action::UpdateArticleList);
                    Util::send(Action::UpdateSidebar);
                }));

                App::default().threadpool().spawn_ok(thread_future);
                Util::glib_spawn_future(glib_future);
            }
        }
    }

    pub fn update_article_header(&self) {
        let (visible_article, visible_article_enclosures) = self
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article();
        let mut updated_visible_article: Option<FatArticle> = None;
        if let Some(visible_article) = visible_article {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Ok(visible_article) = news_flash.get_fat_article(&visible_article.article_id) {
                    updated_visible_article = Some(visible_article);
                }
            }
        }

        if let Some(updated_visible_article) = updated_visible_article {
            self.content_page()
                .articleview_column()
                .show_article(Some(&updated_visible_article), visible_article_enclosures.as_ref());
            self.content_page()
                .articleview_column()
                .article_view()
                .update_visible_article(Some(updated_visible_article.unread), None);
        }
    }

    pub fn update_features(&self) {
        self.content_page().sidebar_column().update_features();
    }
}
