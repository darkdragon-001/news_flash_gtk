use crate::settings::Settings;
use glib::object::IsA;
use gtk4::{prelude::*, Builder, ShortcutsWindow, Window};
use std::str;

pub const XML_DATA: &str = r#"
<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <requires lib="gtk" version="4.0"/>
  <object class="GtkShortcutsWindow" id="shortcuts-window">
    <property name="resizable">0</property>
    <property name="modal">1</property>
    <child>
      <object class="GtkShortcutsSection">
        <property name="max-height">12</property>
        <property name="section-name">news-flash</property>
        <property name="title" translatable="yes">NewsFlash Shortcuts</property>
        <child>
          <object class="GtkShortcutsGroup">
            <property name="title" translatable="yes">Global</property>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Shortcut Window</property>
                <property name="accelerator">$SHORTCUT</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Refresh Feeds</property>
                <property name="accelerator">$REFRESH</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Search</property>
                <property name="accelerator">$SEARCH</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Quit</property>
                <property name="accelerator">$QUIT</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">All Articles</property>
                <property name="accelerator">$ALLARTICLES</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Only Unread Articles</property>
                <property name="accelerator">$ONLYUNREAD</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Only Starred Articles</property>
                <property name="accelerator">$ONLYSTARRED</property>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkShortcutsGroup">
            <property name="title" translatable="yes">Article List</property>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Next Article</property>
                <property name="accelerator">$NEXTART</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Previous Article</property>
                <property name="accelerator">$PREVART</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Toggle Read</property>
                <property name="accelerator">$TOGGLEREAD</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Toggle Starred</property>
                <property name="accelerator">$TOGGLEMARKED</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Open In Browser</property>
                <property name="accelerator">$OPENBROWSER</property>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkShortcutsGroup">
            <property name="title" translatable="yes">Article View</property>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Scroll Up</property>
                <property name="accelerator">$SCROLLUP</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Scroll Down</property>
                <property name="accelerator">$SCROLLDOWN</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Scrape content</property>
                <property name="accelerator">$SCRAPCONTENT</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Tag Article</property>
                <property name="accelerator">$TAGARTICLE</property>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkShortcutsGroup">
            <property name="title" translatable="yes">Feed List</property>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Next Item</property>
                <property name="accelerator">$NEXTFEED</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Previous Item</property>
                <property name="accelerator">$PREVFEED</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Expand/Collapse Category</property>
                <property name="accelerator">$TOGGLEEXPAND</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="title" translatable="yes">Mark Item Read</property>
                <property name="accelerator">$ITEMREAD</property>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
  </object>
</interface>
"#;

pub struct ShortcutsDialog {
    pub widget: ShortcutsWindow,
}

impl ShortcutsDialog {
    pub fn new<D: IsA<Window> + GtkWindowExt>(parent: &D, settings: &Settings) -> Self {
        let mut ui_xml = XML_DATA.to_owned();
        ui_xml = Self::setup_shortcut(&ui_xml, "$SHORTCUT", settings.get_keybind_shortcut());
        ui_xml = Self::setup_shortcut(&ui_xml, "$REFRESH", settings.get_keybind_refresh());
        ui_xml = Self::setup_shortcut(&ui_xml, "$SEARCH", settings.get_keybind_search());
        ui_xml = Self::setup_shortcut(&ui_xml, "$QUIT", settings.get_keybind_quit());
        ui_xml = Self::setup_shortcut(&ui_xml, "$ALLARTICLES", settings.get_keybind_all_articles());
        ui_xml = Self::setup_shortcut(&ui_xml, "$ONLYUNREAD", settings.get_keybind_only_unread());
        ui_xml = Self::setup_shortcut(&ui_xml, "$ONLYSTARRED", settings.get_keybind_only_starred());
        ui_xml = Self::setup_shortcut(&ui_xml, "$NEXTART", settings.get_keybind_article_list_next());
        ui_xml = Self::setup_shortcut(&ui_xml, "$PREVART", settings.get_keybind_article_list_prev());
        ui_xml = Self::setup_shortcut(&ui_xml, "$TOGGLEREAD", settings.get_keybind_article_list_read());
        ui_xml = Self::setup_shortcut(&ui_xml, "$TOGGLEMARKED", settings.get_keybind_article_list_mark());
        ui_xml = Self::setup_shortcut(&ui_xml, "$OPENBROWSER", settings.get_keybind_article_list_open());
        ui_xml = Self::setup_shortcut(&ui_xml, "$SCROLLUP", settings.get_keybind_article_view_up());
        ui_xml = Self::setup_shortcut(&ui_xml, "$SCROLLDOWN", settings.get_keybind_article_view_down());
        ui_xml = Self::setup_shortcut(&ui_xml, "$SCRAPCONTENT", settings.get_keybind_article_view_scrap());
        ui_xml = Self::setup_shortcut(&ui_xml, "$TAGARTICLE", settings.get_keybind_article_view_tag());
        ui_xml = Self::setup_shortcut(&ui_xml, "$NEXTFEED", settings.get_keybind_feed_list_next());
        ui_xml = Self::setup_shortcut(&ui_xml, "$PREVFEED", settings.get_keybind_feed_list_prev());
        ui_xml = Self::setup_shortcut(
            &ui_xml,
            "$TOGGLEEXPAND",
            settings.get_keybind_feed_list_toggle_expanded(),
        );
        ui_xml = Self::setup_shortcut(&ui_xml, "$ITEMREAD", settings.get_keybind_sidebar_set_read());

        let builder = Builder::from_string(&ui_xml);
        let widget: ShortcutsWindow = builder
            .object("shortcuts-window")
            .expect("Failed to create ShortcutsWindow");
        widget.set_transient_for(Some(parent));

        Self { widget }
    }

    fn setup_shortcut(xml: &str, needle: &str, shortcut: Option<String>) -> String {
        match shortcut {
            Some(shortcut) => {
                let shortcut = shortcut.replace("&", "&amp;");
                let shortcut = shortcut.replace("<", "&lt;");
                let shortcut = shortcut.replace(">", "&gt;");
                xml.replacen(needle, &shortcut, 1)
            }
            None => xml.replacen(needle, "", 1),
        }
    }
}
