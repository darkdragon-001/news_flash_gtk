use failure::Fail;
use gtk4::prelude::*;
use gtk4::{subclass::prelude::*, Box, CompositeTemplate, Label, Orientation, Separator, Widget, Window as GtkWindow};
use libadwaita::{subclass::prelude::*, Window as AdwWindow};
use news_flash::{NewsFlash, NewsFlashError};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/error_detail_dialog.ui")]
    pub struct ErrorDialog {
        #[template_child]
        pub list_box: TemplateChild<Box>,
    }

    impl Default for ErrorDialog {
        fn default() -> Self {
            Self {
                list_box: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ErrorDialog {
        const NAME: &'static str = "ErrorDialog";
        type Type = super::ErrorDialog;
        type ParentType = AdwWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ErrorDialog {}

    impl WidgetImpl for ErrorDialog {}

    impl WindowImpl for ErrorDialog {}

    impl AdwWindowImpl for ErrorDialog {}
}

glib::wrapper! {
    pub struct ErrorDialog(ObjectSubclass<imp::ErrorDialog>)
        @extends Widget, GtkWindow, AdwWindow;
}

impl ErrorDialog {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create ErrorDialog")
    }

    pub fn set_error(&self, error: &NewsFlashError) {
        if let Some(backtrace) = error.backtrace() {
            log::error!("backtrace: {}", backtrace);
        }

        let imp = imp::ErrorDialog::from_instance(self);

        let causes = <dyn Fail>::iter_chain(error);
        for (i, cause) in causes.enumerate() {
            let mut string = format!("{}", cause);

            if let Some(error) = NewsFlash::parse_error(cause) {
                if error != string {
                    string = format!("{} ({})", string, error);
                }
            }

            let index_label = Label::new(None);
            index_label.set_text(&format!("{}", i,));
            index_label.set_size_request(30, 0);

            let message_label = Label::new(None);
            message_label.set_text(&string);
            message_label.set_hexpand(true);
            message_label.set_ellipsize(pango::EllipsizeMode::End);
            message_label.set_xalign(0.0);

            let v_separator = Separator::new(Orientation::Vertical);

            let h_box = Box::new(Orientation::Horizontal, 5);
            h_box.set_size_request(0, 30);
            h_box.set_margin_start(5);
            h_box.set_margin_end(5);
            h_box.prepend(&index_label);
            h_box.prepend(&v_separator);
            h_box.prepend(&message_label);

            if i != 0 {
                let separator = Separator::new(Orientation::Horizontal);
                imp.list_box.prepend(&separator);
            }

            imp.list_box.prepend(&h_box);
        }

        self.present();
    }
}
